import express from 'express';
import { requireSignIn, requireAuth} from '../../services/passport';
import UserController from '../../controllers/user/user.controller';
import { multerSaveTo } from '../../services/multer-service';
import { parseStringToArrayOfObjectsMw } from '../../utils';

const router = express.Router();


router.post('/signin',UserController.signIn);
router.post('/socialLogin',UserController.socialLogin);
router.route('/signup')
    .post(  
        multerSaveTo('users').single('img'),
        UserController.validateUserCreateBody(),
        UserController.signUp
    );

router.route('/addStore')
    .post(
        multerSaveTo('users').fields([
            { name: 'img', maxCount: 1, options: false },
            { name: 'storeImages', maxCount: 10, options: false }
        ]),
        parseStringToArrayOfObjectsMw('discounts'),
        parseStringToArrayOfObjectsMw('rules'),
        parseStringToArrayOfObjectsMw('category'),
        parseStringToArrayOfObjectsMw('subCategory'),
        UserController.validateStoreCreateBody(),
        UserController.signUpStore
    );
router.route('/:qrCode/getUser')
    .get(
        requireAuth,
        UserController.findByQrCode
    );

router.route('/:id/getUserById')
    .get(
        requireAuth,
        UserController.findById
    );
router.route('/:userId/block')
    .put(
        requireAuth,
        UserController.block
    );

router.route('/:userId/unblock')
    .put(
        requireAuth,
        UserController.unblock
    );

router.route('/:userId/updateSalesCode')
    .put(
        requireAuth,
        UserController.updateSalesCode
    );


router.route('/logout')
    .post(
        requireAuth,
        UserController.logout
    );
router.route('/addToken')
    .post(
        requireAuth,
        UserController.addToken
    );
router.route('/updateToken')
    .put(
        requireAuth,
        UserController.updateToken
    );


router.put('/check-exist-email', UserController.checkExistEmail);

router.put('/check-exist-phone', UserController.checkExistPhone);

router.put('/user/:userId/updateInfo',
    requireAuth,
    multerSaveTo('users').fields([
        { name: 'img', maxCount: 1, options: false },
        { name: 'storeImages', maxCount: 10, options: false }

    ]),
    parseStringToArrayOfObjectsMw('discounts'),
    parseStringToArrayOfObjectsMw('rules'),
    parseStringToArrayOfObjectsMw('category'),
    parseStringToArrayOfObjectsMw('subCategory'),
    UserController.validateUpdatedBody(true),
    UserController.updateInfo);


router.put('/user/:userId/updateAvatar',
    requireAuth,
    multerSaveTo('users').single('img'),
    UserController.updateAvatar);

router.put('/user/updatePassword',
    requireAuth,
    UserController.validateUpdatedPassword(),
    UserController.updatePassword);

router.post('/sendCode',
    UserController.validateSendCode(),
    UserController.sendCodeToEmail);

router.post('/confirm-code',
    UserController.validateConfirmVerifyCode(),
    UserController.resetPasswordConfirmVerifyCode);


router.post('/sendCode-phone',
    UserController.validateForgetPassword(),
    UserController.forgetPasswordSms);

router.post('/sendCode-phone-twillo',
    UserController.validateForgetPassword(),
    UserController.sendSmsTwillo);

router.post('/confirm-code-phone',
    UserController.validateConfirmVerifyCodePhone(),
    UserController.resetPasswordConfirmVerifyCodePhone);

router.post('/reset-password-phone',
    UserController.validateResetPasswordPhone(),
    UserController.resetPasswordPhone);
 

router.post('/reset-password',
    UserController.validateResetPassword(),
    UserController.resetPassword);

router.route('/findAll')
    .get(UserController.findAllWithoutToken)

router.route('/getAll')
    .get(requireAuth,UserController.findAll)
router.route('/getAll/withoutPagenation')
    .get(requireAuth,UserController.findAllWithoutPagenation)
router.route('/getAllBySalesCode')
    .get(requireAuth,UserController.findUsersBySalesCode)

router.route('/getTopRate')
    .get(UserController.findTopRate)

router.route('/findTopRateWithToken')
    .get(requireAuth,UserController.findTopRateWithToken)

router.route('/:userId/delete')
    .delete(requireAuth,UserController.delete)
router.route('/:packageId/buyPackage')
    .put(requireAuth,UserController.buyPackage)

    
router.route('/acceptPay')
.post(UserController.acceptPay)

router.route('/:userId/active')
    .put(
        requireAuth,
        UserController.active
    );
router.route('/:userId/dis-active')
    .put(
        requireAuth,
        UserController.disactive
    );

router.route('/:userId/show')
    .put(
        requireAuth,
        UserController.show
    );
router.route('/:userId/hide')
    .put(
        requireAuth,
        UserController.hide
    );

export default router;
