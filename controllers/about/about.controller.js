import ApiResponse from "../../helpers/ApiResponse";
import About from "../../models/about/about.model";
import Report from "../../models/reports/report.model";
import ApiError from '../../helpers/ApiError';

import { checkExist, checkExistThenGet, isImgUrl } from "../../helpers/CheckMethods";
import { handleImg, checkValidations } from "../shared/shared.controller";
import { body } from "express-validator/check";

export default {

    async findAll(req, res, next) {

        try {
            let query = {deleted: false };
            let about = await About.find(query)

            res.send(about);
        } catch (err) {
            next(err);
        }
    },

    validateBody(isUpdate = false) {
        let validations = [
            body('about').not().isEmpty().withMessage('about is required'),
            body('terms').not().isEmpty().withMessage('terms is required'),
            body('privacy').not().isEmpty().withMessage('privacy is required'),

        ];
        
        return validations;
    },

    async create(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
    
            const validatedBody = checkValidations(req);
           
            let createdAbout = await About.create({ ...validatedBody});

            let reports = {
                "action":"Create About Us",
            };
            let report = await Report.create({...reports, user: user });
            res.status(201).send(createdAbout);
        } catch (err) {
            next(err);
        }
    },

    async update(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { aboutId } = req.params;
            await checkExist(aboutId, About, { deleted: false });

            const validatedBody = checkValidations(req);
          
            let updatedAbout = await About.findByIdAndUpdate(aboutId, {
                ...validatedBody,
            }, { new: true });
            let reports = {
                "action":"Update About Us",
            };
            let report = await Report.create({...reports, user: user });
            res.status(200).send(updatedAbout);
        }
        catch (err) {
            next(err);
        }
    },
   
    async delete(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
                
            let { aboutId } = req.params;
            let about = await checkExistThenGet(aboutId, About, { deleted: false });
            about.deleted = true;
            await about.save();
            
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },
};