import mongoose,{ Schema} from "mongoose";
import autoIncrement from 'mongoose-auto-increment';
const AboutSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    about: {
        type: String,
        trim: true,
        required: true,
    },
    terms: {
        type: String,
        trim: true,
        required: true,
    },
    privacy: {
        type: String,
        trim: true,
        required: true,
    },
   
    deleted:{
        type:Boolean,
        default:false
    }
}, { timestamps: true });

AboutSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
AboutSchema.plugin(autoIncrement.plugin, { model: 'about', startAt: 1 });

export default mongoose.model('about', AboutSchema);