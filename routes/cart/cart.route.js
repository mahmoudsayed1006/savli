import express from 'express';
import CartController from '../../controllers/cart/cart.controller';
import { requireAuth } from '../../services/passport';

const router = express.Router();

router.route('/:productId/products')
    .post(
        requireAuth,
        CartController.create
    );
router.route('/:userId/users')
    .get(CartController.findAll);
    

router.route('/:productId')
    .delete( requireAuth,CartController.unCart);





export default router;