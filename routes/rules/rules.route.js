import express from 'express';
import RulesController from '../../controllers/rules/rules.controller';
import { requireAuth } from '../../services/passport';

const router = express.Router();

router.route('/')
    .post(
        requireAuth,
        RulesController.validateBody(),
        RulesController.create
    )
    .get(RulesController.findAll);

router.route('/withoutPagenation/get')
    .get(RulesController.getAll);
 
router.route('/:ruleId')
    .put(
        requireAuth,
        RulesController.validateBody(true),
        RulesController.update
    )
    .get(RulesController.findById)
    .delete( requireAuth,RulesController.delete);

export default router;