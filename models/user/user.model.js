import mongoose, { Schema } from 'mongoose';
import autoIncrement from 'mongoose-auto-increment';
import bcrypt from 'bcryptjs';
import isEmail from 'validator/lib/isEmail';
import { isImgUrl } from "../../helpers/CheckMethods";



const userSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    fullname: {
        type: String,
        
    },
    email: {
        type: String,
        trim: true,
        //required: true,
    },
    phone: {
        type:String,
        //required: true,
        trim: true,
    },
    username: {
        type:String,
        trim: true,
    },
    password: { 
        type: String,
        trim: true,
        //required: true
    },
    socialId:{
        type: String,
    },
    signUpFrom: {
        type: String,
        enum: ['SOCAIL','NORMAL'],
        default:'NORMAL',
        required:true
    },
    type: {
        type: String,
        enum: ['USER','STORE','SUB-STORE', 'ADMIN'],
        required:true
    },
    city:{
        type:Number,
        ref:'city',
        required:true
    },
    area:{
        type:Number,
        ref:'area',
        required:true
    },
    category:{
        type:[Number],
        ref:'category',
    },
    subCategory:{
        type:[Number],
        ref:'category',
    },
    favourite:{
        type:[Number],
        ref:'user',
    },
    carts:{
        type:[Number],
        ref:'cart',
    },
    parentUser:{
        type:Number,
        ref:'user',
    },
    img: {
        type: String,
    },
    store:{//الفرع وانا بضيف كاشير
        type:Number,
        ref:'user',
    },

    branchName_ar:{
        type: String,
        default:"فرع رئيسى"
    },
    branchName_en:{
        type: String,
        default:"center branch"
    },
    storeType:{//نوع المحل رئيسى ولا فرعى
        type: String,
        enum:["main","branch"],
        default:"main"
    },
    mainStore:{ //الفرع الرئيسى
        type:Number,
        ref:'user',
    },
    salesCode:{
        type: String
    },
    salesCodeType:{//نوع كود البيع
        type: String,
        enum:["self","main"],
        default:"self"
    },

    storePhone:{
        type:String,
        trim: true,
    },
    storeImages: {
        type: [String],
    },
    storeName: {
        type: String,
    },
    storeName_ar: {
        type: String,
        default:"",

    },
    storeAbout: {
        type: String,
    },
    location: { 
        type: [String],
    },
    rateCount: {
        type: Number,
        default:0
    },
    rateNumbers: {
        type: Number,
        default:0
    },
    rate: {
        type: Number,
        default:0
    },
    billsCount:{
        type: Number,
        default:0
    },
    distance:{
        type: Number
    },
    address: {
        type: String,
    },
    workTime: {
        type: String,
    },
    discounts: [
        new Schema({
            package: {
                type: Number,
                required: true,
            },
            discount: {
                type: Number,
                required: true,
            },
           
        }, { _id: false })
        
    ],
    /*
    rules: [
        new Schema({
            description:{
                type:String,
                required:true
            },
            discount:{
                type:Number,
                required:true
            },
            package: {
                type: Number,
                required: true,
                ref:'package'
            },
           
        }, { _id: false })
        
    ],
    //*/
    rules:{
        type:[Number],
        ref:'rule',
    },
    hasRules:{
        type:Boolean,
        default:false
    },
    userDiscount: [
        new Schema({
            package: {
                type: Number,
                required: true,
            },
            discount: {
                type: Number,
                required: true,
            },
           
        }, { _id: false })
        
    ],
    //package
    package:{
        type: Number,
        ref: 'package'
    },
    unActivePackage:{
        type: Number,
        ref: 'package'
    },
    billReference:{
        type: Number,
        default:0
    },
    freeOperationsNum:{
        type: Number,
        default:0
    },
    /*hasPackage: {
        type: Boolean,
        default: false
    },*/
    packageStart:{
        type: Date,
    },
    packageEnd:{
        type: Date,
    },
    points: {
        type: Number,
        default:0
    },
    verifycode: {
        type: Number
    },
    token:{
        type:[String],
    },
    block: {
        type: Boolean,
        default: false
    },
    visible: {
        type: Boolean,
        default: true
    },
    active:{
        type:Boolean,
        default:false
    },
    isFavourite:{
        type:Boolean,
        default:false
    },
    isLogin:{
        type:Boolean,
        default:false
    },
    balance:{
        type: Number,
        default:0
    },
    totalBalance:{
        type: Number,
        default:0
    },
    deleted: {
        type: Boolean,
        default: false
    }
}, { timestamps: true, discriminatorKey: 'kind' });

userSchema.pre('save', function (next) {
    const account = this;
    if (!account.isModified('password')) return next();

    const salt = bcrypt.genSaltSync();
    bcrypt.hash(account.password, salt).then(hash => {
        account.password = hash;
        next();
    }).catch(err => console.log(err));
});

userSchema.methods.isValidPassword = function (newPassword, callback) {
    let user = this;
    bcrypt.compare(newPassword, user.password, function (err, isMatch) {
        if (err)
            return callback(err);
        callback(null, isMatch);
    });
};

userSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret.password;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
userSchema.plugin(autoIncrement.plugin, { model: 'user', startAt: 1 });
userSchema.plugin(autoIncrement.plugin, { model: 'user', field: 'qrCode', startAt: 10001  });

export default mongoose.model('user', userSchema);