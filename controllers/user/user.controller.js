import { checkExistThenGet, checkExist } from '../../helpers/CheckMethods';
import { body } from 'express-validator/check';
import { checkValidations, handleImg, handleImgs,distance } from '../shared/shared.controller';
import { generateToken } from '../../utils/token';
import User from "../../models/user/user.model";
import Report from "../../models/reports/report.model";
import ApiError from '../../helpers/ApiError';
import { sendForgetPassword } from '../../services/message-service';
import { sendMsg } from '../../services/sms-misr';
import uniqid from 'uniqid';
import { sendEmail } from "../../services/emailMessage.service";
import { toImgUrl } from "../../utils";
import { generateVerifyCode } from '../../services/generator-code-service';
import Notif from "../../models/notif/notif.model";
import ApiResponse from "../../helpers/ApiResponse";
import bcrypt from 'bcryptjs';
import { sendNotifiAndPushNotifi } from "../../services/notification-service";
import Package from "../../models/package/package.model";

const populateQuery = [ 
    { path: 'city', model: 'city' },
    { path: 'mainStore', model: 'user' },
    { path: 'store', model: 'user' },
    { path: 'area', model: 'area' },
    { path: 'package', model: 'package' },
    { path: 'category', model: 'category' },
    { path: 'subCategory', model: 'category' },
    {
        path: 'store', model: 'user',
        populate: { path: 'city', model: 'city' },
    },
    {
        path: 'store', model: 'user',
        populate: { path: 'area', model: 'area' },
    },
    {
        path: 'store', model: 'user',
        populate: { path: 'category', model: 'category' },
    },
    {
        path: 'store', model: 'user',
        populate: { path: 'subCategory', model: 'category' },
    },
];
const checkUserExistByPhone = async (phone) => {
    let user = await User.findOne({ phone });
    if (!user)
        throw new ApiError.BadRequest('Phone Not Found');

    return user;
}
const checkUserExistByEmail = async (email) => {
    let user = await User.findOne({ email });
    if (!user)
        throw new ApiError.BadRequest('email Not Found');

    return user;
}

export default {
    async addToken(req,res,next){
        try{
            let user = req.user;
            let users = await checkExistThenGet(user.id, User);
            let arr = users.token;
            var found = arr.find(function(element) {
                return element == req.body.token;
            });
            if(!found){
                users.token.push(req.body.token);
                await users.save();
            console.log(req.body.token);
            }
            res.status(200).send({
                users,
            });
            
        } catch(err){
            next(err);
        }
    },
    async signIn(req, res, next) {
        try{
            
            /*let user = req.user;
            user = await User.findById(user.id).populate(populateQuery);
            if(!user)
                return next(new ApiError(403, ('username or password incorrect')));
            */
            if(req.body.phone == undefined )
                 return next(new ApiError(400)); 
            let user 
            user = await User.findOne({ username:req.body.phone}).populate(populateQuery);
            console.log(user)
            if(!user)
            user = await User.findOne({ phone:req.body.phone}).populate(populateQuery);
            if(!user)
            return next(new ApiError(403, ('incorrect data')));   
            if (bcrypt.compareSync(req.body.password, user.password)) {
                user.isLogin = true;
                let curr = Date.parse(new Date());
                console.log(curr)
                let to = Date.parse(user.packageEnd)
                console.log(to)
                if(curr > to ){
                    let packages = await Package.find({deleted:false,defaultPackage:true});
                    let packageId = packages[0].id
                    if(packages.length > 0){
                        let packages = await checkExistThenGet(packageId, Package, { deleted: false });
                    
                        let theUser = await checkExistThenGet(user.id, User,{deleted: false });
                        theUser.package = packages.id;
                        var date = new Date();
                        var endDate = new Date(date.setMonth(date.getMonth() + packages.month));
                        theUser.packageStart = new Date();
                        theUser.packageEnd = endDate;
                        await theUser.save();
                    } 
                }
                if(!user.package && user.type =="USER"){
                    let packages = await Package.find({deleted:false,defaultPackage:true});
                    let packageId = packages[0].id
                    if(packages.length > 0){
                        let packages = await checkExistThenGet(packageId, Package, { deleted: false });
                    
                        let theUser = await checkExistThenGet(user.id, User,{deleted: false });
                        theUser.package = packages.id;
                        var date = new Date();
                        var endDate = new Date(date.setMonth(date.getMonth() + packages.month));
                        theUser.packageStart = new Date();
                        theUser.packageEnd = endDate;
                        await theUser.save();
                    } 
                }
                
                if(req.body.token != null && req.body.token !=""){
                    let arr = user.token; 
                    var found = arr.find(function(element) {
                        return element == req.body.token;
                    });
                    if(!found){
                        user.token.push(req.body.token);
                        user.save();
                    }
                }
                if(user.block == true){
                    return next(new ApiError(403, ('sorry you are blocked')));
                }
                if(user.deleted == true){
                    return next(new ApiError(403, ('sorry you are deleted')));
                }

                
                res.status(200).send({
                    user:await User.findById(user.id).populate(populateQuery),
                    token: generateToken(user.id)
                });
                
                let reports = {
                    "action":"User Login",
                };

                let report = await Report.create({...reports, user: user });
            } else{
                return next(new ApiError(403, ('password incorrect'))); 
            }
            
        } catch(err){
            next(err);
        }
    },
    

    validateUserCreateBody(isUpdate = false) {
        let validations = [
            body('store').optional(),
            body('username').optional()
            .custom(async (value, { req }) => {
                let userQuery = { username: value ,deleted:false};
                if (isUpdate && req.user.username === value)
                    userQuery._id = { $ne: req.user._id };

                if (await User.findOne(userQuery))
                    throw new Error(req.__('username duplicated'));
                else
                    return true;
            }),
            body('city').not().isEmpty().withMessage('city is required')
                .isNumeric().withMessage('city should be number'),
            body('area').not().isEmpty().withMessage('area is required')
                .isNumeric().withMessage('area should be number'),
            body('fullname').not().isEmpty().withMessage('fullname is required'),
            body('phone').not().isEmpty().withMessage('phone is required')
                .custom(async (value, { req }) => {
                    let userQuery = { phone: value ,deleted:false};
                    if (isUpdate && req.user.phone === value)
                        userQuery._id = { $ne: req.user._id };

                    if (await User.findOne(userQuery))
                        throw new Error(req.__('phone duplicated'));
                    else
                        return true;
                }),
                
            body('email').optional()
                .custom(async (value, { req }) => {
                    let userQuery = { email: value ,deleted:false};
                    if (isUpdate && req.user.email === value)
                        userQuery._id = { $ne: req.user._id };

                    if (await User.findOne(userQuery))
                        throw new Error(req.__('email duplicated'));
                    else
                        return true;
                }),

            body('type').not().isEmpty().withMessage('type is required')
            .isIn(['USER','STORE','SUB-STORE','ADMIN']).withMessage('wrong type'),
            body('img').optional().custom(val => isImgUrl(val)).withMessage('img should be a valid img'),
            body('salesCode').optional(),
            body('socialId').optional()
            .custom(async (value, { req }) => {
                let userQuery = { socialId: value ,deleted:false};
                if (isUpdate && req.user.socialId === value)
                    userQuery._id = { $ne: req.user._id };

                if (await User.findOne(userQuery))
                    throw new Error(req.__('socialId duplicated'));
                else
                    return true;
            }),
            

        ];
        if (!isUpdate) {
            validations.push([
                body('password').optional()
            ]);
        }
        return validations;
    },
    async signUp(req, res, next) {
        try {
            const validatedBody = checkValidations(req);
            if(validatedBody.socialId){
                validatedBody.signUpFrom = 'SOCAIL';
            }
            if(validatedBody.salesCode){
               
                let salesCode = validatedBody.salesCode.toUpperCase()
                if(!await User.findOne({deleted:false,salesCode:salesCode}))
                     return next(new ApiError(500, ('wrong sales code')));
                let parentUser = await User.findOne({deleted:false,salesCode:salesCode});
                validatedBody.parentUser = parentUser.id
            }
            if (req.file) {
               let image = await handleImg(req)
               validatedBody.img = image;
            }
            
            let createdUser = await User.create({
                ...validatedBody,token:req.body.token
            });
            if(validatedBody.type == "USER"){
                let packages = await Package.find({deleted:false,defaultPackage:true});
                let packageId = packages[0].id
                
                if(packages.length > 0){
                    let packages = await checkExistThenGet(packageId, Package, { deleted: false });
                   
                    let user = await checkExistThenGet(createdUser.id, User,{deleted: false });
                    user.package = packages.id;
                    var date = new Date();
                    var endDate = new Date(date.setMonth(date.getMonth() + packages.month));
                    user.packageStart = new Date();
                    user.packageEnd = endDate;
                    await user.save();
                } 
            }
            let user = await checkExistThenGet(createdUser.id, User,{deleted: false });
            let code =  generateVerifyCode(); 
            if(code.toString().length < 4){
                code = generateVerifyCode(); 
            }else{
                user.verifycode = code
            }
            console.log(code)
            let str = uniqid.time();
            let salesCode = str.toUpperCase()
            user.salesCode = salesCode 
            await user.save();
            //send code
            console.log(user.verifycode)
            let message =  '  رمز التفعيل الخاص بك هو :  ' + user.verifycode
            sendMsg(message,user.phone)
            res.status(201).send({
                user: await User.findById(createdUser.id).populate(populateQuery),
                token: generateToken(createdUser.id)
            });
           
            let reports = {
                "action":"User Sign Up",
            };
            let report = await Report.create({...reports, user: createdUser.id });

        } catch (err) {
            next(err);
        }
    },
    async updateSalesCode(req, res, next) {
        try {
            if ( req.user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { userId} = req.params;
            let user = await checkExistThenGet(userId,User);
            let str = uniqid.time();
            let salesCode = str.toUpperCase()
            user.salesCode = salesCode 
            await user.save();
            res.send({user});
        } catch (error) {
            next(error);
        }
    },
    validateStoreCreateBody(isUpdate = false) {
        let validations = [
            body('store').optional(),
            body('storePhone').optional(),
            body('storeName_ar').not().isEmpty().withMessage('arabic storeName is required'),
            body('username').optional().custom(async (value, { req }) => {
                let userQuery = { username: value,deleted:false };
                if (isUpdate && req.user.username === value)
                    userQuery._id = { $ne: req.user._id };

                if (await User.findOne(userQuery))
                    throw new Error(req.__('username duplicated'));
                else
                    return true;
            }),
            body('workTime').optional(),
            body('lat').optional(),
            body('lang').optional(),
            body('address').optional(),
            body('storeAbout').optional(),
            body('category').not().isEmpty().withMessage('category is required'),
            body('subCategory').not().isEmpty().withMessage('sub Category is required'),
            body('city').not().isEmpty().withMessage('city is required')
                .isNumeric().withMessage('city should be number'),
            body('area').not().isEmpty().withMessage('area is required')
                .isNumeric().withMessage('area should be number'),
            body('storeName').not().isEmpty().withMessage('storeName is required'),
            body('phone').not().isEmpty().withMessage('phone is required')
                .custom(async (value, { req }) => {
                    let userQuery = { phone: value,deleted:false };
                    if (isUpdate && req.user.phone === value)
                        userQuery._id = { $ne: req.user._id };

                    if (await User.findOne(userQuery))
                        throw new Error(req.__('phone duplicated'));
                    else
                        return true;
                }),
            body('email').optional()
                .custom(async (value, { req }) => {
                    let userQuery = { email: value,deleted:false };
                    if (isUpdate && req.user.email === value)
                        userQuery._id = { $ne: req.user._id };

                    if (await User.findOne(userQuery))
                        throw new Error(req.__('email duplicated'));
                    else
                        return true;
                }),
            body('discounts')
                .isLength({ min: 1 }).withMessage('discounts should have at least one element of discounts')
                .custom(async (properties, { req }) => {
                    for (let prop of properties) {
                        body('package').not().isEmpty().withMessage('package is required'),
                        body('discount').not().isEmpty().withMessage('discount is required')
                    }
                    return true;
                }),
            body('rules').optional()
                /*.custom(async (properties, { req }) => {
                    for (let prop of properties) {
                        body('description').not().isEmpty().withMessage('description is required'),
                        body('package').not().isEmpty().withMessage('package is required'),
                        body('discount').not().isEmpty().withMessage('discount is required')
                    }
                    return true;
                })*/,
            body('type').not().isEmpty().withMessage('type is required')
            .isIn(['USER','STORE','ADMIN']).withMessage('wrong type'),
            body('img').optional().custom(val => isImgUrl(val)).withMessage('img should be a valid img'),
            body('salesCode').optional(),
            body('branchName_ar').optional(),
            body('branchName_en').optional(),
            body('storeType').optional().isIn(['main','branch']).withMessage('wrong store Type'),
            body('mainStore').optional(),
            body('salesCodeType').optional().isIn(['main','self']).withMessage('wrong salesCodeType'),
        ];
        if (!isUpdate) {
            validations.push([
                body('password').not().isEmpty().withMessage('password is required')
            ]);
        }
        return validations;
    },
    async signUpStore(req, res, next) {
        try {
            const validatedBody = checkValidations(req);
            validatedBody.location = [validatedBody.lat,validatedBody.lang]
            console.log(validatedBody.salesCode)
            if(validatedBody.salesCode){
                let salesCode = validatedBody.salesCode.toUpperCase()
                if(!await User.findOne({deleted:false,salesCode:salesCode}))
                     return next(new ApiError(500, ('wrong sales code')));
                let parentUser = await User.findOne({deleted:false,salesCode:salesCode});
                validatedBody.parentUser = parentUser.id
            }
            if (req.files) {
                if (req.files['img']) {
                    let imagesList = [];
                    for (let imges of req.files['img']) {
                        imagesList.push(await toImgUrl(imges))
                    }
                    validatedBody.img = imagesList;
                }
                if (req.files['storeImages']) {
                    let imagesList = [];
                    for (let imges of req.files['storeImages']) {
                        imagesList.push(await toImgUrl(imges))
                    }
                    validatedBody.storeImages = imagesList;
                }
                
            }
            let createdUser = await User.create({
                ...validatedBody,token:req.body.token
            });
            let user = await checkExistThenGet(createdUser.id, User,{deleted: false });
            let str = uniqid.time();
            let salesCode = str.toUpperCase()
            user.salesCode = salesCode
            await user.save();

            
            res.status(201).send({
                user: await User.findById(createdUser.id).populate(populateQuery),
                token: generateToken(createdUser.id)
            });
            
            let reports = {
                "action":"add Store",
            };
            let report = await Report.create({...reports, user: createdUser.id });

        } catch (err) {
            next(err);
        }
    },
    
    
    async block(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { userId} = req.params;
            let blockUser = await checkExistThenGet(userId,User);
            blockUser.block = true;
            await blockUser.save();
            let reports = {
                "action":"block User",
            };
            let report = await Report.create({...reports, user: user });
            res.send('user block');
        } catch (error) {
            next(error);
        }
    },
    async unblock(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { userId} = req.params;
            let blockUser = await checkExistThenGet(userId,User);
            blockUser.block = false;
            await blockUser.save();
            let reports = {
                "action":"Active User",
            };
            let report = await Report.create({...reports, user: user });
            res.send('user active');
        } catch (error) {
            next(error);
        }
    },
    async active(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { userId} = req.params;
            let activeUser = await checkExistThenGet(userId,User);
            activeUser.active = true;
            activeUser.deleted = false;
            await activeUser.save();
            let reports = {
                "action":"Active User",
            };
            let report = await Report.create({...reports, user: user });
            res.send('user active');
            
        } catch (error) {
            next(error);
        }
    },

    async disactive(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { userId} = req.params;
            let activeUser = await checkExistThenGet(userId,User);
            activeUser.active = false;
            await activeUser.save();
            let reports = {
                "action":"Dis-Active User",
            };
            let report = await Report.create({...reports, user: user });
            res.send('user disactive');
        } catch (error) {
            next(error);
        }
    },
    async show(req, res, next) {
        try {
            if (req.user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { userId} = req.params;
            let user = await checkExistThenGet(userId,User);
            user.visible = true;
            await user.save();
            let reports = {
                "action":"show User",
            };
            let report = await Report.create({...reports, user: req.user });
            res.send('user visible');
            
        } catch (error) {
            next(error);
        }
    },

    async hide(req, res, next) {
        try {
            if (req.user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { userId} = req.params;
            let user = await checkExistThenGet(userId,User);
            user.visible = false;
            await user.save();
            let reports = {
                "action":" User hide",
            };
            let report = await Report.create({...reports, user: req.user });
            res.send('user hide');
            
        } catch (error) {
            next(error);
        }
    },

    async findById(req, res, next) {
        try {
            let { id } = req.params;
            await checkExist(id, User, { deleted: false });
            let theUser = await checkExistThenGet(id, User)
             //check package 
             let curr = Date.parse(new Date());
             console.log(curr)
             let to = Date.parse(theUser.packageEnd)
             console.log(to)
             if(curr > to){
                 let packages = await Package.find({deleted:false,defaultPackage:true});
                 let packageId = packages[0].id
                 if(packages.length > 0){
                     let packages = await checkExistThenGet(packageId, Package, { deleted: false });
                 
                     let theUser = await checkExistThenGet(theUser.id, User,{deleted: false });
                     theUser.package = packages.id;
                     var date = new Date();
                     var endDate = new Date(date.setMonth(date.getMonth() + packages.month));
                     theUser.packageStart = new Date();
                     theUser.packageEnd = endDate;
                     await theUser.save();
                 } 
             }

            res.status(200).send({
                user:await User.findById(id).populate(populateQuery),
                token: generateToken(id)
            });
            
        } catch (error) {
            next(error);
        }
    },
    async findByQrCode(req, res, next) {
        try {
            if (req.user.type == 'USER')
            return next(new ApiError(403, ('store.auth')));

            let { qrCode } = req.params;
            let user = await User.findOne({qrCode:qrCode,deleted: false});
            if(!await User.findOne({qrCode:qrCode,deleted: false}))
                  return next(new ApiError(500, ('user not found wrong qr code')));


            
            //check package 
            let curr = Date.parse(new Date());
            console.log(curr)
            let to = Date.parse(user.packageEnd)
            console.log(to)
            if(curr > to){
                let packages = await Package.find({deleted:false,defaultPackage:true});
                let packageId = packages[0].id
                if(packages.length > 0){
                    let packages = await checkExistThenGet(packageId, Package, { deleted: false });
                
                    let theUser = await checkExistThenGet(user.id, User,{deleted: false });
                    theUser.package = packages.id;
                    var date = new Date();
                    var endDate = new Date(date.setMonth(date.getMonth() + packages.month));
                    theUser.packageStart = new Date();
                    theUser.packageEnd = endDate;
                    await theUser.save();
                } 
            }

            let storeId = req.user._id;
            if(req.user.type == "STORE"){
                storeId = req.user._id;
            }else{
                storeId = req.user.store;
            }

            let store = await checkExistThenGet(storeId, User, { deleted: false });
            let packagee = await checkExistThenGet(user.package, Package, { deleted: false });         
                let arr = store.discounts
                let obj = arr.find(o => o.package == user.package);
                let index = arr.indexOf(obj);
                let discount = arr[index].discount;
                console.log(discount)
            user = await User.findOne({qrCode:qrCode,deleted: false}).populate(populateQuery);

            if(packagee.defaultPackage == true){
                if(user.freeOperationsNum >= packagee.freeOperations ){
                    return next(new ApiError(400, ('sorry this user finish all free operation')));
                }
            }
            res.send({user,discount:discount})
        } catch (error) {
            next(error);
        }
    },

    async checkExistEmail(req, res, next) {
        try {
            let email = req.body.email;
            if (!email) {
                return next(new ApiError(400, 'email is required'));
            }
            let exist = await User.findOne({ email: email });
            let duplicated;
            if (exist == null) {
                duplicated = false;
            } else {
                duplicated = true
            }
            let reports = {
                "action":"User Check Email Exist Or Not",
            };
            let report = await Report.create({...reports, user: req.user });
            return res.status(200).send({ 'duplicated': duplicated });
        } catch (error) {
            next(error);
        }
    },

    async checkExistPhone(req, res, next) {
        try {
            let phone = req.body.phone;
            if (!phone) {
                return next(new ApiError(400, 'phone is required'));
            }
            let exist = await User.findOne({ phone: phone });
            let duplicated;
            if (exist == null) {
                duplicated = false;
            } else {
                duplicated = true
            }
            let reports = {
                "action":"User Check Mobile Exist Or Not",
            };
            let report = await Report.create({...reports, user: req.user });
            return res.status(200).send({ 'duplicated': duplicated });
        } catch (error) {
            next(error);
        }
    },
    validateUpdatedBody(isUpdate = true) {
        let validations = [
            body('lat').optional(),
            body('lang').optional(),
            body('storePhone').optional(),
            body('storeName_ar').optional(),
            body('password').optional(),
            body('store').optional(),
            body('username').optional().custom(async (value, { req }) => {
                let {userId} = req.params;
                let user = await checkExistThenGet(userId, User);
                let userQuery = { username: value,deleted:false };
                if (isUpdate && user.username === value)
                    userQuery._id = { $ne: userId };
                
                if (await User.findOne(userQuery))
                    throw new Error(req.__('username duplicated'));
                else
                    return true;
            }),
            body('freeOperationsNum').optional(),
            body('storeName').optional(),
            body('fullname').optional(),
            body('workTime').optional(),
            body('address').optional(),
            body('storeAbout').optional(),
            body('category').optional(),
            body('subCategory').optional(),
            body('city').optional()
                .isNumeric().withMessage('city should be number'),
            body('area').optional()
                .isNumeric().withMessage('area should be number'),
            
          
            body('email').optional()
                .custom(async (value, { req }) => {
                    let {userId} = req.params;
                    let user = await checkExistThenGet(userId, User);
                    let userQuery = { email: value,deleted:false };
                    if (isUpdate && user.email === value)
                        userQuery._id = { $ne: userId };
                    
                    if (await User.findOne(userQuery))
                        throw new Error(req.__('email duplicated'));
                    else
                        return true;
            }),
            body('phone').optional()
            .custom(async (value, { req }) => {
                let {userId} = req.params;
                let user = await checkExistThenGet(userId, User);
                let userQuery = { phone: value,deleted:false };
                if (isUpdate && user.phone === value)
                    userQuery._id = { $ne: userId };
                
                if (await User.findOne(userQuery))
                    throw new Error(req.__('phone duplicated'));
                else
                    return true;
        }),
            body('type').optional()
            .isIn(['USER','STORE','ADMIN']).withMessage('wrong type'),
            body('discounts').optional()
            .isLength({ min: 1 }).withMessage('discounts should have at least one element of discounts')
            .custom(async (properties, { req }) => {
                for (let prop of properties) {
                    body('package').not().isEmpty().withMessage('area is required'),
                    body('discount').not().isEmpty().withMessage('area is required')
                }
                return true;
            }),
            body('rules').optional()
               /* .custom(async (properties, { req }) => {
                    for (let prop of properties) {
                        body('description').not().isEmpty().withMessage('description is required'),
                        body('package').not().isEmpty().withMessage('package is required'),
                        body('discount').not().isEmpty().withMessage('discount is required')
                    }
                    return true;
                })*/,
            body('branchName_ar').optional(),
            body('branchName_en').optional(),
            body('storeType').optional().isIn(['main','branch']).withMessage('wrong store Type'),
            body('mainStore').optional(),
            body('salesCodeType').optional().isIn(['main','self']).withMessage('wrong salesCodeType'),
        ];
        if (isUpdate)
        validations.push([
            body('img').optional().custom(val => isImgUrl(val)).withMessage('img should be a valid img')
        ]);

        return validations;
    },
    async updateInfo(req, res, next) {
        try {
            let {userId} = req.params;
            const validatedBody = checkValidations(req);
            let user = await checkExistThenGet(userId, User);
            if(validatedBody.lat && validatedBody.lang){
                validatedBody.location = [validatedBody.lat,validatedBody.lang]
                user.location = validatedBody.location;
            }
            if (req.files) {
                if (req.files['img']) {
                    let imagesList = [];
                    for (let imges of req.files['img']) {
                        imagesList.push(await toImgUrl(imges))
                    }
                    user.img = imagesList;
                }
                if (req.files['storeImages']) {
                    let imagesList = [];
                    for (let imges of req.files['storeImages']) {
                        imagesList.push(await toImgUrl(imges))
                    }
                    user.storeImages = imagesList;
                }
                
            }
            if(validatedBody.rules){
                user.rules = validatedBody.rules
            }
            if(validatedBody.branchName_ar){
                user.branchName_ar = validatedBody.branchName_ar;
            }
            if(validatedBody.branchName_en){
                user.branchName_en = validatedBody.branchName_en;
            }
            if(validatedBody.storeType){
                user.storeType = validatedBody.storeType;
            }
            if(validatedBody.mainStore){
                user.mainStore = validatedBody.mainStore;
            }
            if(validatedBody.salesCodeType){
                user.salesCodeType = validatedBody.salesCodeType;
            }
            if(validatedBody.freeOperationsNum){
                user.freeOperationsNum = validatedBody.freeOperationsNum;
            }
            if(validatedBody.discounts){
                user.discounts = validatedBody.discounts;
            }
            if(validatedBody.username){
                user.username = validatedBody.username;
            }
            if(validatedBody.email){
                user.email = validatedBody.email;
            }
            if(validatedBody.phone){
                user.phone = validatedBody.phone;
            }
            if(validatedBody.fullname){
                user.fullname = validatedBody.fullname;
            }
           
            if(validatedBody.city){
                user.city = validatedBody.city;
            }
            if(validatedBody.area){
                user.area = validatedBody.area;
            }
            if(validatedBody.category){
                user.category = validatedBody.category;
            }
            if(validatedBody.subCategory){
                user.subCategory = validatedBody.subCategory;
            }
           
            if(validatedBody.storeName){
                user.storeName = validatedBody.storeName;
            }
            if(validatedBody.storeAbout){
                user.storeAbout = validatedBody.storeAbout;
            }
            if(validatedBody.address){
                user.address = validatedBody.address;
            }
            if(validatedBody.workTime){
                user.workTime = validatedBody.workTime;
            }
            if(validatedBody.password){
                user.password = validatedBody.password;
            }
            if(validatedBody.storePhone){
                user.storePhone = validatedBody.storePhone;
            }
            if(validatedBody.storeName_ar){
                user.storeName_ar = validatedBody.storeName_ar;
            }
           
           
            await user.save();
            let reports = {
                "action":"Update User Info",
            };
            let report = await Report.create({...reports, user: req.user});
            res.status(200).send({
                user: await User.findById(userId).populate(populateQuery)
            });

        } catch (error) {
            next(error);
        }
    }, 
    validateUpdatedPassword(isUpdate = false) {
        let validation = [
            body('newPassword').optional().not().isEmpty().withMessage('newPassword is required'),
            body('currentPassword').optional().not().isEmpty().withMessage('currentPassword is required')
           
        ];

        return validation;
    },
    async updatePassword(req, res, next) {
        try {
            let user = await checkExistThenGet(req.user._id, User);

            if (req.body.newPassword) {
                if (req.body.currentPassword) {
                    if (bcrypt.compareSync(req.body.currentPassword, user.password)) {
                        user.password = req.body.newPassword;
                    }
                    else {
                        res.status(400).send({
                            error: [
                                {
                                    location: 'body',
                                    param: 'currentPassword',
                                    msg: 'currentPassword is invalid'
                                }
                            ]
                        });
                    }
                }
            }
            await user.save();
            res.status(200).send({
                user: await User.findById(req.user._id)
            });

        } catch (error) {
            next(error);
        }
    },
    async updateAvatar(req, res, next) {
        try {
            let {userId} = req.params;
            const validatedBody = {};
            let user = await checkExistThenGet(userId, User);
            if (req.file) {
                let image = await handleImg(req, { attributeName: 'img', isUpdate: true });
                validatedBody.img = image;
            }
            if(validatedBody.img){
                user.img = validatedBody.img;
            }
           
            await user.save();
            res.status(200).send({
                user: await User.findById(userId)
            });

        } catch (error) {
            next(error);
        }
    }, 
    
    validateSendCode() {
        return [
            body('email').not().isEmpty().withMessage('email Required')
        ];
    },
    async sendCodeToEmail(req, res, next) {
        try {
            let validatedBody = checkValidations(req);
            let user = await checkUserExistByEmail(validatedBody.email);
            let code =  generateVerifyCode(); 
            if(code.toString().length < 4){
                code = generateVerifyCode(); 
            }else{
                user.verifycode = code
            }
            await user.save();
            //send code
            let text = user.verifycode.toString();
            let description = 'Savli verfication code';
            sendEmail(validatedBody.email, text,description)
            res.status(204).send();
        } catch (error) {
            next(error);
        }
    },
    validateConfirmVerifyCode() {
        return [
            body('verifycode').not().isEmpty().withMessage('verifycode Required'),
            body('email').not().isEmpty().withMessage('email Required'),
        ];
    },
    async resetPasswordConfirmVerifyCode(req, res, next) {
        try {
            let validatedBody = checkValidations(req);
            let user = await checkUserExistByEmail(validatedBody.email);
            if (user.verifycode != validatedBody.verifycode)
                return next(new ApiError.BadRequest('verifyCode not match'));
            user.active = true;
            await user.save();
           
            res.status(204).send();
        } catch (err) {
            next(err);
        }
    },

    validateResetPassword() {
        return [
            body('email').not().isEmpty().withMessage('email is required'),
            body('newPassword').not().isEmpty().withMessage('newPassword is required')
        ];
    },

    async resetPassword(req, res, next) {
        try {

            let validatedBody = checkValidations(req);
            let user = await checkUserExistByEmail(validatedBody.email);

            user.password = validatedBody.newPassword;
            user.verifyCode = '0000';
            await user.save();
           
            res.status(204).send();

        } catch (err) {
            next(err);
        }
    },
    validateForgetPassword() {
        return [
            body('phone').not().isEmpty().withMessage('Phone Required')
        ];
    },
    async sendSmsTwillo(req, res, next) {
        try {
            let validatedBody = checkValidations(req);
            let realPhone = validatedBody.phone;
            let user = await checkUserExistByPhone(validatedBody.phone);

            let code =  generateVerifyCode(); 
            console.log(code.toString().length)
            if(code.toString().length < 4){
                code = generateVerifyCode(); 
            }else{
                user.verifycode = code
            }
            await user.save();
            //send sms
            let phone = '+2' + realPhone
            sendForgetPassword(user.verifycode, phone);
            let reports = {
                "action":"send verify code to user",
            };
            let report = await Report.create({...reports, user: req.user });
            res.status(204).send();
            
        } catch (error) {
            next(error);
        }
    },
    async forgetPasswordSms(req, res, next) {
        try {
            let validatedBody = checkValidations(req);
            let realPhone = validatedBody.phone;
            let user = await checkUserExistByPhone(validatedBody.phone);

            let code =  generateVerifyCode(); 
            console.log(code.toString().length)
            if(code.toString().length < 4){
                code = generateVerifyCode(); 
            }else{
                user.verifycode = code
            }
            await user.save();
            //send sms
            //sendForgetPassword(user.verifycode, realPhone);
            let message =  ' رمز التفعيل الخاص بك هو  ' + user.verifycode
            //sendMsg(message,realPhone)
            let reports = {
                "action":"send verify code to user",
            };
            let report = await Report.create({...reports, user: req.user });
            res.status(204).send();
            
        } catch (error) {
            next(error);
        }
    },
    validateConfirmVerifyCodePhone() {
        return [
            body('phone').not().isEmpty().withMessage('phone Required'),
            body('verifycode').not().isEmpty().withMessage('verifycode Required'),
        ];
    },
    async resetPasswordConfirmVerifyCodePhone(req, res, next) {
        try {
            let validatedBody = checkValidations(req);
            let user = await checkUserExistByPhone(validatedBody.phone);
            if (user.verifycode != validatedBody.verifycode)
                return next(new ApiError.BadRequest('verifyCode not match'));
            user.active = true;
            await user.save();
            res.status(204).send();
        } catch (err) {
            next(err);
        }
    },
    validateResetPasswordPhone() {
        return [
            body('phone').not().isEmpty().withMessage('phone is required'),
            body('newPassword').not().isEmpty().withMessage('newPassword is required')
        ];
    },

    async resetPasswordPhone(req, res, next) {
        try {

            let validatedBody = checkValidations(req);
            let user = await checkUserExistByPhone(validatedBody.phone);
            user.password = validatedBody.newPassword;
            await user.save();
            let reports = {
                "action":"User reset Password",
            };
            let report = await Report.create({...reports, user: req.user });
            res.status(204).send();

        } catch (err) {
            next(err);
        }
    },


    async updateToken(req,res,next){
        try{
            let users = await checkExistThenGet(req.user._id, User);
            let arr = users.token;
            var found = arr.find(function(element) {
                return element == req.body.newToken;
            });
            if(!found){
                users.token.push(req.body.newToken);
                await users.save();
            }
            let oldtoken = req.body.oldToken;
            console.log(arr);
            for(let i = 0;i<= arr.length;i=i+1){
                if(arr[i] == oldtoken){
                    arr.splice(arr[i], 1);
                }
            }
            users.token = arr;
            await users.save();
            res.status(200).send(await checkExistThenGet(req.user._id, User));
        } catch(err){
            next(err)
        }
    },
    async logout(req,res,next){
        try{
            let users = await checkExistThenGet(req.user._id, User);
            let arr = users.token;
            let token = req.body.token;
            console.log(arr);
            for(let i = 0;i<= arr.length;i=i+1){
                if(arr[i] == token){
                    arr.splice(arr[i], 1);
                }
            }
            users.token = arr;
            users.isLogin = false;
            await users.save();
            res.status(200).send(await checkExistThenGet(req.user._id, User));
        } catch(err){
            next(err)
        }
    },
    async findAll(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            {mainStore,storeType,km,lat,lang,orderByKm,name,orderByRate,orderByBills,block,type,city,area,category,subCategory,visible,active,store,parentUser} = req.query;
            let query = {deleted: false};
            if(name) {
                query = {
                    $and: [
                        { $or: [
                            {storeName: { $regex: '.*' + name + '.*' , '$options' : 'i'  }}, 
                            {storeName_ar: { $regex: '.*' + name + '.*', '$options' : 'i'  }}, 
                            {fullname: { $regex: '.*' + name + '.*' , '$options' : 'i'  }},

                          ] 
                        },
                        {deleted: false},
                    ]
                };
            }
            if (mainStore) query.mainStore = mainStore;
            if (storeType) query.storeType = storeType;
            if (block =="true")
                query.block = true;
            if (block =="false")
                query.block = false;

            if (active =="true")
                query.active = true;
            if (active =="false")
                query.active = false;

            if (visible =="true")
                query.visible = true;
            if (visible =="false")
                query.visible = false;
	        if (parentUser) query.parentUser = parentUser;
            if (store) query.store = store;
            if (type) query.type = type;
            if (area) query.area = area;
            if (category) query.category = category;
            if (city) query.city = city;
            if (subCategory) query.subCategory = subCategory;
            let sortd = { createdAt: -1 }
            if(orderByBills){
                sortd = { billsCount: -1 }
            }
            if(orderByRate){
                sortd = { rate: -1 }
            }
            if(orderByKm){
                sortd = {distance:1}
            }
            let users = await User.find(query)
            let myUser = await checkExistThenGet(req.user._id, User)
            let data =[];
            for (let user of users) {
                let theUser = await checkExistThenGet(user._id, User)
                    let curr = Date.parse(new Date())
                    let packages = await Package.find({deleted:false,defaultPackage:true});
                    let packageId = packages[0].id
            
                    if(curr > theUser.packageEnd){
                        theUser.package = packageId;
                    }
                    let arr = myUser.favourite;
                    var found = arr.find(function(element) {
                        return element == user._id;
                    });
                    if(found){
                        theUser.isFavourite = true
                    } else{
                        theUser.isFavourite = false
                    }
                    if(theUser.type =="STORE"){
                        let destination = theUser.location;
                        if(theUser.location.length > 0 && destination[0] != null){
                            if(lat && lang){
                                let numberOfKm = distance(lat,lang,parseFloat(destination[0]),parseFloat(destination[1]),"K")
                                theUser.distance = parseFloat(numberOfKm);
                                if(km){
                                    let numberOfKmBetween = distance(lat,lang,parseFloat(destination[0]),parseFloat(destination[1]),"K")
                                    if(numberOfKmBetween < km){
                                        data.push(user)
                                    }
                                }
                            }
                        }
                        if(myUser.type =="USER"){
                            let arr = theUser.discounts

                            let index
                            let obj = arr.find((o, i) => {
                                if (o.package == myUser.package) {
                                    index = i
                                }
                            });
                            let userDiscount = arr[index]
                            theUser.userDiscount = userDiscount
                        }
                    }
                await theUser.save();
            }
            
            if(km){
                const dataCount = data.length;
                const page2Count = Math.ceil(dataCount / limit);
                let theDate = data.slice((page - 1) * limit, page * limit);
                
                res.send(new ApiResponse(theDate, page, page2Count, limit, dataCount, req));
                
            }else{
                users = await User.find(query).populate(populateQuery)
                .sort(sortd)
                .limit(limit)
                .skip((page - 1) * limit);
                const usersCount = await User.count(query);
                const pageCount = Math.ceil(usersCount / limit);
                res.send(new ApiResponse(users, page, pageCount, limit, usersCount, req));
            }
            
        } catch (err) {
            next(err);
        }
    },
    async findAllWithoutToken(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            {mainStore,storeType,km,lat,lang,orderByKm,name,orderByRate,orderByBills, block,type,city,area,category,subCategory,active,visible,store,parentUser} = req.query;
            let query = {deleted: false};
            if(name) {
                query = {
                    $and: [
                        { $or: [
                            {storeName: { $regex: '.*' + name + '.*' , '$options' : 'i'  }}, 
                            {storeName_ar: { $regex: '.*' + name + '.*' , '$options' : 'i'  }}, 
                            {fullname: { $regex: '.*' + name + '.*' , '$options' : 'i'  }},

                          ] 
                        },
                        {deleted: false},
                    ]
                };
            }
            if (mainStore) query.mainStore = mainStore;
            if (storeType) query.storeType = storeType;
            if (block =="true")
                query.block = true;
            if (block =="false")
                query.block = false;

            if (active =="true")
                query.active = true;
            if (active =="false")
                query.active = false;

            if (visible =="true")
                query.visible = true;
            if (visible =="false")
                query.visible = false;

            if (type) query.type = type;
	        if (parentUser) query.parentUser = parentUser;
            if (store) query.store = store;
            if (area) query.area = area;
            if (category) query.category = category;
            if (city) query.city = city;
            if (subCategory) query.subCategory = subCategory;

            let sortd = { createdAt: -1 }
            if(orderByBills){
                sortd = { billsCount: -1 }
            }
            if(orderByRate){
                sortd = { rate: -1 }
            }
            if(orderByKm){
                sortd = {distance:1}
            }
            let users = await User.find(query).populate(populateQuery)
                
            let data =[]
            for (let user of users) {
                let theUser = await checkExistThenGet(user._id, User)
                if(theUser.img != undefined){
                    let newImg = theUser.img.replace('//savli', '//api.savli');
                    theUser.img = newImg;
                    console.log(newImg)
                }
                /*if(theUser.storeImages.length >0){
                    let storeImages = []
                    theUser.storeImages.forEach(img => {
                        let newImg = img.replace('//savli', '//api.savli');
                        storeImages.push(newImg)
                    });
                    theUser.storeImages = storeImages;
                    console.log(storeImages)
                }*/
                

                if(theUser.type =="STORE"){

                    let destination = theUser.location;
                    if(theUser.location.length > 0 && destination[0] != null){
                        if(lat && lang){
                            let numberOfKm = distance(lat,lang,parseFloat(destination[0]),parseFloat(destination[1]),"K")
                            theUser.distance = parseFloat(numberOfKm);
                            if(km){
                                let numberOfKmBetween = distance(lat,lang,parseFloat(destination[0]),parseFloat(destination[1]),"K")
                                if( numberOfKmBetween < km){
                                    data.push(user)
                                }
                            }
                        }
                    }
                }

                await theUser.save()
            } 
            if(km){
                const dataCount = data.length;
                const page2Count = Math.ceil(dataCount / limit);
                let theDate = data.slice((page - 1) * limit, page * limit);
                
                res.send(new ApiResponse(theDate, page, page2Count, limit, dataCount, req));
                
            }else{
                users = await User.find(query).populate(populateQuery)
                .sort(sortd)
                .limit(limit)
                .skip((page - 1) * limit);
                const usersCount = await User.count(query);
                const pageCount = Math.ceil(usersCount / limit);
                res.send(new ApiResponse(users, page, pageCount, limit, usersCount, req));
            }
        } catch (err) {
            next(err);
        }
    },
    async findAllWithoutPagenation(req, res, next) {
        try {
            let {mainStore,storeType,km,lat,lang,orderByKm,name,orderByRate,orderByBills, block,type,city,area,category,subCategory,active,visible,store,parentUser} = req.query;
            let query = {deleted: false};
            if(name) {
                query = {
                    $and: [
                        { $or: [
                            {storeName: { $regex: '.*' + name + '.*' , '$options' : 'i'  }}, 
                            {storeName_ar: { $regex: '.*' + name + '.*' , '$options' : 'i'  }}, 
                            {fullname: { $regex: '.*' + name + '.*' , '$options' : 'i'  }},
                          ] 
                        },
                        {deleted: false},
                    ]
                };
            }
            if (mainStore) query.mainStore = mainStore;
            if (storeType) query.storeType = storeType;
            if (block =="true")
                query.block = true;
            if (block =="false")
                query.block = false;

            if (active =="true")
                query.active = true;
            if (active =="false")
                query.active = false;

            if (visible =="true")
                query.visible = true;
            if (visible =="false")
                query.visible = false;

            if (type) query.type = type;
	        if (parentUser) query.parentUser = parentUser;
            if (store) query.store = store;
            if (area) query.area = area;
            if (category) query.category = category;
            if (city) query.city = city;
            if (subCategory) query.subCategory = subCategory;

            let sortd = { createdAt: -1 }
            if(orderByBills){
                sortd = { billsCount: -1 }
            }
            if(orderByRate){
                sortd = { rate: -1 }
            }
            if(orderByKm){
                sortd = {distance:1}
            }
            let users = await User.find(query).populate(populateQuery)
                
            let data =[]
            for (let user of users) {
                let theUser = await checkExistThenGet(user._id, User)
               
                if(theUser.type =="STORE"){

                    let destination = theUser.location;
                    if(theUser.location.length > 0 && destination[0] != null){
                        if(lat && lang){
                            let numberOfKm = distance(lat,lang,parseFloat(destination[0]),parseFloat(destination[1]),"K")
                            theUser.distance = parseFloat(numberOfKm);
                            if(km){
                                let numberOfKmBetween = distance(lat,lang,parseFloat(destination[0]),parseFloat(destination[1]),"K")
                                if( numberOfKmBetween < km){
                                    data.push(user)
                                }
                            }
                        }
                    }
                    if(theUser.location[0] == null){
                        theUser.location = [0,0]
                    }
                }

                await theUser.save()
            } 
            if(km){
                res.send(theDate);
                
            }else{
                users = await User.find(query).populate(populateQuery)
                .sort(sortd)
                res.send(users);
            }
        } catch (err) {
            next(err);
        }
    },
    async findByLocation(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            {mainStore,storeType, type,category,km} = req.query;
            let query = {deleted:false}
            if (type)
                query.type = type;
            if (category){
                query.category = category;
            } 
            if (mainStore) query.mainStore = mainStore;
            if (storeType) query.storeType = storeType;
            
            let data = [];

            let users = await User.find(query).populate(populateQuery).sort({ createdAt: -1 })
            if(users.length >0) {
                for(var i=0;i<users.length;i++){
                    if(users[i].destination){
                        let numberOfKm = distance(lat,lang,users[i].destination[0],users[i].destination[1],"K")
                        console.log(numberOfKm)
                        if(numberOfKm < km){
                            data.push(users[i])
                        }
                    }
                }
            }
            const usersCount = data.length;
            const pageCount = Math.ceil(usersCount / limit);

            res.send(new ApiResponse(data, page, pageCount, limit, usersCount, req));
            res.send(data);
        } catch (err) {
            next(err);
        }
    },
    
    async findTopRate(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit ,
            { mainStore,storeType,type,city,area,category,subCategory,active,visible} = req.query;
            let query = {deleted: false,type:"STORE",block:false };
            if (active =="true")
                query.active = true;
            if (active =="false")
                query.active = false;

            if (visible =="true")
                query.visible = true;
            if (visible =="false")
                query.visible = false;
                
            if (mainStore) query.mainStore = mainStore;
            if (storeType) query.storeType = storeType;
            
            if (area) query.area = area;
            if (category) query.category = category;
            if (city) query.city = city;
            if (subCategory) query.subCategory = subCategory;
            let users = await User.find(query).populate(populateQuery)
                .sort({ rate: -1 })
            if(limit){
                users = await User.find(query).populate(populateQuery)
                .sort({ rate: -1 })
                .limit(limit)
            }
            res.send(users);
        } catch (err) {
            next(err);
        }

    },
    async findTopRateWithToken(req, res, next) {
        try {
            
            let page = +req.query.page || 1, limit = +req.query.limit ,
            {mainStore,storeType, type,city,area,category,subCategory,active,visible} = req.query;
            let query = {deleted: false,type:"STORE",block:false };
            if (active =="true")
                query.active = true;
            if (active =="false")
                query.active = false;
            if (mainStore) query.mainStore = mainStore;
            if (storeType) query.storeType = storeType;
            if (visible =="true")
                query.visible = true;
            if (visible =="false")
                query.visible = false;
            if (area) query.area = area;
            if (category) query.category = category;
            if (city) query.city = city;
            if (subCategory) query.subCategory = subCategory;
            let users = await User.find(query).populate(populateQuery)
                .sort({ rate: -1 })
            if(limit){
                users = await User.find(query).populate(populateQuery)
                .sort({ rate: -1 })
                .limit(limit)
            }
            let myUser = await checkExistThenGet(req.user._id, User)
            
                for await (let user of users) {
                    let theUser = await checkExistThenGet(user._id, User)
                        let curr = Date.parse(new Date())
                        let packages = await Package.find({deleted:false,defaultPackage:true});
                        let packageId = packages[0].id
                        console.log("step1");
                        if(curr > theUser.packageEnd){
                            theUser.package = packageId;
                        }
                        let arr = myUser.favourite;
                        var found = arr.find(function(element) {
                            return element == user._id;
                        });
                        if(found){
                            theUser.isFavourite = true
                        } else{
                            theUser.isFavourite = false
                        }
                        if(theUser.type =="STORE"){
                            if(myUser.type =="USER"){
                                let arr = theUser.discounts
                                console.log("step2");
                                console.log("arr:",arr);
                                let index
                                let obj = arr.find((o, i) => {
                                    if (o.package == myUser.package) {
                                        index = i
                                    }
                                });
                                console.log(index);
                                let userDiscount = arr[index]
                                console.log(userDiscount)
                                theUser.userDiscount = userDiscount
                                console.log("step3");
                            }
                            
                        }
                        delete theUser.__v
                        await theUser.save(function(err, doc) {
                            if (err) return console.error(err);
                            console.log("succussfully!");
                          });
                }
                
                console.log("step4");
            users = await User.find(query).populate(populateQuery)
                .sort({ rate: -1 })
            if(limit){
                users = await User.find(query).populate(populateQuery)
                .sort({ rate: -1 })
                .limit(limit)
            }
            res.send(users);
        } catch (err) {
            next(err);
        }
    },
    async findUsersBySalesCode(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            { salesCode, daily} = req.query;
            var mydate = new Date().toISOString().slice(0, 10);           
            let from = mydate + 'T00:00:00.000Z';
            let to= mydate + 'T23:59:00.000Z';
            let query = {deleted: false };
            let user 
            if (salesCode) {
                query.salesCode = salesCode;
                user = await User.findOne(query);
                if(!await User.findOne(query))
                return next(new ApiError(500, ('wronge sales Code')));
                console.log(user)
            }
            

            let query2 = {
                deleted:false
            }
            if(daily){
                console.log('hhh')
                query2 = {
                    $and: [
                        {createdAt: { $gte : new Date(from), $lte : new Date(to) }},
                        {deleted: false } 
                    ]
                }
            }
            if(salesCode){
                query2.parentUser = user
            }else{
                query2.parentUser = undefined
            }
            

            let users = await User.find(query2).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);
            const usersCount = await User.count(query2);
            const pageCount = Math.ceil(usersCount / limit);

            res.send(new ApiResponse(users, page, pageCount, limit, usersCount, req));
        } catch (err) {
            next(err);
        }
    },
    async delete(req, res, next) {
        try {
            let { userId } = req.params;
            let user = await checkExistThenGet(userId, User);
            user.deleted = true;
            /*let ads = await Ads.find({owner : userId });
            for (let add of ads ) {
                add.deleted = true;
                await add.save();
            }*/
            await user.save();
           // await User.findByIdAndDelete(userId);
            let reports = {
                "action":"Delete user",
            };
            let report = await Report.create({...reports, user: req.user });
            res.status(204).send();
        } catch (error) {
            next(error)
        }
    },
   
    async buyPackage(req, res, next) {
        try {
            let {packageId } = req.params;
            let packages = await checkExistThenGet(packageId, Package, { deleted: false });
            packages.usersCount = packages.usersCount + 1;
            await packages.save();
            let user = await checkExistThenGet(req.user._id, User,{deleted: false });
            user.unActivePackage = packageId;
            user.billReference = req.body.billReference;
            var date = new Date();
            var endDate = new Date(date.setMonth(date.getMonth() + packages.month));
           // user.packageStart = new Date();
            //user.packageEnd = endDate;
            //user.hasPackage = true;
        
            await user.save();
                
            res.send(user);
        }
        catch (err) {
            next(err);
        }
    },
    async acceptPay(req, res, next) {
        try {
            console.log(req.body.obj.id)
            if(!await User.findOne({deleted:false,billReference:req.body.obj.id}))
                return next(new ApiError(500, ('not a correct bill reference')));
            let user = await User.findOne({deleted:false,billReference:req.body.obj.id});
            let theUser = await checkExistThenGet(user.id, User,{deleted: false });
            console.log(theUser)
            let packages = await checkExistThenGet(theUser.unActivePackage, Package, { deleted: false });
            var date = new Date();
            var endDate = new Date(date.setMonth(date.getMonth() + packages.month));
            theUser.package = theUser.unActivePackage
            theUser.packageStart = new Date();
            theUser.packageEnd = endDate;
            theUser.billReference = 0
            await theUser.save();
            if(theUser.parentUser){
                let parentUser = await checkExistThenGet(theUser.parentUser, User,{deleted: false });
               
                if(parentUser.type =="STORE"){
                    let addtionBalance = (packages.cost * packages.ratioForParentStore) / 100
                    parentUser.balance = parentUser.balance + addtionBalance
                    parentUser.totalBalance = parentUser.totalBalance + addtionBalance
                }else{
                    let addtionBalance = (packages.cost * packages.ratioForParentUser) / 100
                    parentUser.balance = parentUser.balance + addtionBalance
                    parentUser.totalBalance = parentUser.totalBalance + addtionBalance
                }
                await parentUser.save();
                

            }
            res.status(204).send();
        } catch (error) {
            next(error)
        }
    },
    async socialLogin(req, res, next) {
        try{
            console.log(req.body.socialId)
            if(req.body.socialId == undefined )
            return next(new ApiError(400)); 
            let user = await User.findOne({socialId:req.body.socialId}).populate(populateQuery);
            console.log(user)
            if(!user)
                 return next(new ApiError(500, ('user not found')));  
            if(req.body.token != null && req.body.token !=""){
                let arr = user.token; 
                var found = arr.find(function(element) {
                    return element == req.body.token;
                });
                if(!found){
                    user.token.push(req.body.token);
                    await user.save();
                }
            }
            res.status(200).send({
                user: await User.findOne({socialId:req.body.socialId}).populate(populateQuery),
                token: generateToken(user.id)
            });
            
        } catch(err){
            next(err);
        }
    }
   
};
