import mongoose,{ Schema} from "mongoose";
import autoIncrement from 'mongoose-auto-increment';
const BillSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    user:{
        type:Number,
        ref:'user'
    },
    store:{
        type:Number,
        ref:'user'
    },
    actionUser:{
        type:Number,
        ref:'user'
    },
    billItems: [
        new Schema({
            price: {
                type: Number,
                required: true,
            },
            priceAfterDiscount:{
                type:Number,
                required:true
            },
            discount: {
                type: Number,
                required: true,
            },
            discountRule: {
                type: Number,
                ref:'rule'
            },
           
        }, { _id: false })
        
    ],
    price:{
        type:Number,
        //required:true
    },
    priceAfterDiscount:{
        type:Number,
        //required:true
    },
    discount:{
        type:Number,
        //required:true
    },
    hasRate:{
        type:Boolean,
        default:false
    },
    cancel:{
        type:Boolean,
        default:false
    },
    orderBill:{
        type:Boolean,
        default:false
    },
    deleted:{
        type:Boolean,
        default:false
    }
}, { timestamps: true });

BillSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
BillSchema.plugin(autoIncrement.plugin, { model: 'bill', startAt: 1 });

export default mongoose.model('bill', BillSchema);