import ApiResponse from "../../helpers/ApiResponse";
import Report from "../../models/reports/report.model";
import ApiError from '../../helpers/ApiError';
import { checkExist, checkExistThenGet} from "../../helpers/CheckMethods";
import { checkValidations } from "../shared/shared.controller";
import { body } from "express-validator/check";
import Package from "../../models/offer package/offerPackage.model";
export default {

    async findAll(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20 ;

            let query = {deleted: false };
            let packages = await Package.find(query)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const packagesCount = await Package.count(query);
            const pageCount = Math.ceil(packagesCount / limit);

            res.send(new ApiResponse(packages, page, pageCount, limit, packagesCount, req));
        } catch (err) {
            next(err);
        }
    },
    async getAll(req, res, next) {

        try {
            let query = {deleted: false };
            let packages = await Package.find(query)
           
            res.send(packages);
        } catch (err) {
            next(err);
        }
    },
   
    validateBody(isUpdate = false) {
        let validations = [
            body('points').not().isEmpty().withMessage('points is required'),
            body('description').optional(),
            body('spacial').optional(),
            
        ];
        return validations;
    },

    async create(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin only')));
    
            const validatedBody = checkValidations(req);
            if(validatedBody.spacial == "true"){
                validatedBody.spacial = true
            }else{
                validatedBody.spacial = false
            }
            let createdpackage = await Package.create({ ...validatedBody});
            
            let reports = {
                "action":"Create  offer package",
            };
            let report = await Report.create({...reports, user: user });
            
            res.status(201).send(createdpackage);
        } catch (err) {
            next(err);
        }
    },


    async findById(req, res, next) {
        try {
            let { packageId } = req.params;
            await checkExist(packageId, Package, { deleted: false });
            let packages = await Package.findById(packageId);
            res.send(packages);
        } catch (err) {
            next(err);
        }
    },
    async update(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
            return next(new ApiError(403, ('admin only')));

            let { packageId } = req.params;
            await checkExist(packageId, Package, { deleted: false });

            const validatedBody = checkValidations(req);
            if(validatedBody.spacial == "true"){
                validatedBody.spacial = true
            }else{
                validatedBody.spacial = false
            }
            let updatedpackage = await Package.findByIdAndUpdate(packageId, {
                ...validatedBody,
            }, { new: true });
            let reports = {
                "action":"Update offer package",
            };
            let report = await Report.create({...reports, user: user });
            res.status(200).send(updatedpackage);
        }
        catch (err) {
            next(err);
        }
    },
    
    async delete(req, res, next) {
        try {
            let user = req.user;
            
            let { packageId } = req.params;
            let packages = await checkExistThenGet(packageId, Package, { deleted: false });
            
            packages.deleted = true;
            await packages.save();
            let reports = {
                "action":"Delete package",
            };
            let report = await Report.create({...reports, user: user });
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },
};