import ApiError from "../../helpers/ApiError";
import User from "../../models/user/user.model";
import City from "../../models/city/city.model";
import Contact from "../../models/contact/contact.model";
import Report from "../../models/reports/report.model";
import Problem from "../../models/problem/problem.model";
import Bill from "../../models/bills/bills.model";
import Package from "../../models/package/package.model";
import Category from "../../models/category/category.model";
import { checkExistThenGet } from '../../helpers/CheckMethods';

const populateQuery = [
    { path: 'client', model: 'user' },
];
const action = [
    { path: 'user', model: 'user' },
]
export default {
    async getLastUser(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, 'bad auth'));
            let query = {
                $and: [
                    {deleted: false},
                    {type:'CLIENT'}
                ]
            };
         
            let lastUser = await User.find(query)
                .sort({ createdAt: -1 })
                .limit(10);

            res.send(lastUser);
        } catch (error) {
            next(error);
        }
    },
    async getLastActions(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, 'bad auth'));
            let query = {deleted: false};
         
            let lastUser = await Report.find(query).populate(action)
                .sort({ createdAt: -1 })
                .limit(10);

            res.send(lastUser);
        } catch (error) {
            next(error);
        }
    },

   
    async count(req,res, next) {
        try {
            const storeCount = await User.count({deleted:false,type:'STORE'});
            const usersCount = await User.count({deleted:false,type:'USER'});
            const adminsCount = await User.count({deleted:false,type:'ADMIN'});
            const citiesCount = await City.count({deleted:false});
            const messages = await Contact.count({deleted:false,reply:false});
            const problemCount = await Problem.count({ deleted: false});
            const packageCount = await Package.count({ deleted: false});
            const billsCount = await Bill.count({ deleted: false});
            const categoriesCount = await Category.count({ deleted: false});
            let totalMoney = await Bill.find({ deleted: false}).select('price');
            var sum = 0;
            for (var i = 0; i < totalMoney.length; i++) { 
              sum += totalMoney[i].price
            }
            let user = await User.find({deleted:false,type:'USER'})
                .sort({ billsCount: -1 })

            let store = await User.find({deleted:false,type:'STORE'})
                .sort({ billsCount: -1 })

            let storeTopRate = await User.find({deleted:false,type:'STORE'})
                .sort({ rate: -1 })
            res.status(200).send({
                adminsCount:adminsCount,
                problemCount:problemCount,
                messages:messages,
                packageCount:packageCount,

                categoriesCount:categoriesCount,
                citiesCount:citiesCount,
                storeCount:storeCount,
                usersCount:usersCount,
                billsCount:billsCount,
                totalMoney:sum,
                userTop:user[0],
                storeTop:store[0],
                storeTopRate:storeTopRate[0]
            });
        } catch (err) {
            next(err);
        }
        
    },
    async countStore(req,res, next) {
        try {
            let {userId} = req.params
            let store = await checkExistThenGet(userId, User, { deleted: false });
            var mydate = new Date().toISOString().slice(0, 10);           
            let from = mydate + 'T00:00:00.000Z';
            let to= mydate + 'T23:59:00.000Z';
            const childCount = await User.count({deleted:false,parentUser:userId});
            const usersCount = await User.count({deleted:false,type:'SUB-STORE',store:userId});
            const billsCount = await Bill.count({ deleted: false,store:userId/*,cancel:false*/});
            let totalProfitMoney = await Bill.find({ deleted: false,store:userId/*,cancel:false*/}).select('priceAfterDiscount');
            var sum = 0;
            for (var i = 0; i < totalProfitMoney.length; i++) { 
              sum += totalProfitMoney[i].priceAfterDiscount
            }
            let query = {
                $and: [
                    {createdAt: { $gte : new Date(from), $lte : new Date(to) }},
                    {deleted: false } ,
                    {store:userId},
                    //{cancel:false}
                ]
            }
            const DailybillsCount = await Bill.count(query);
            let query2 = {
                $and: [
                    {createdAt: { $gte : new Date(from), $lte : new Date(to) }},
                    {deleted: false } ,
                    {store:userId},
                    //{cancel:false}
                ]
            }
            let DailtProfitMoney = await Bill.find(query2).select('priceAfterDiscount');
            var sum2 = 0;
            for (var i = 0; i < DailtProfitMoney.length; i++) { 
              sum2 += DailtProfitMoney[i].priceAfterDiscount
            }
            let date = new Date()
            let newDate = date.toISOString().slice(0, 8)  
            let firstDay = newDate + '01T00:00:00.000Z';
            //get days in month
            function daysInMonth (month, year) {
                return new Date(year, month , 0).getDate();
            }

            let DaysInCurrentMonth = daysInMonth(date.getMonth() + 1,date.getFullYear()); //number of day in month
            //last day
            let lastDay1= newDate + DaysInCurrentMonth+'T23:59:00.000Z';
            console.log(lastDay1)
            //get month name
            //get days in month
            function theMonth (month, year) {
                const datee = new Date(year, month , 0); 
                return datee.getMonth()
            }
            let month1 = theMonth(date.getMonth()+2,date.getFullYear())
            if(month1 == 0){
                month1 = 12
            }
            console.log(month1)
            let firstDay1 = date.getFullYear()+'-'+ month1 + '-01T00:00:00.000Z';
            if(month1 > 0 && month1 < 10){
                firstDay1 = date.getFullYear()+'-0'+ month1 + '-01T00:00:00.000Z';
            }
            let query3 = {
                $and: [
                    {createdAt: { $gte : new Date(firstDay1), $lte : new Date(lastDay1)}},
                    {deleted: false } ,
                    {store:userId},
                    //{cancel:false}
                ]
            }
            const MonthlybillsCount = await Bill.count(query3);
            let query4 = {
                $and: [
                    {createdAt: { $gte : new Date(firstDay1), $lte : new Date(lastDay1) }},
                    {deleted: false } ,
                    {store:userId},
                    //{cancel:false}
                ]
            }
            let MonthlyProfitMoney = await Bill.find(query4).select('priceAfterDiscount');
            var sum3 = 0;
            for (var i = 0; i < MonthlyProfitMoney.length; i++) { 
              sum3 += MonthlyProfitMoney[i].priceAfterDiscount
            }
            let sum4 = 0
            let activeChildCount = await User.find({deleted:false,parentUser:userId})
            .populate('package');
            activeChildCount.forEach(child => {
                console.log(child)
                if(child.package.defaultPackage != true &&  Date.parse(lastDay1) > Date.parse(child.packageStart) > Date.parse(firstDay1) ){
                    sum4 = sum4 + 1
                }
            });
           
            res.status(200).send({
                ChildCount:childCount,
                BillsCount:billsCount,
                UsersCount:usersCount,
                TotalProfitMoney:sum,
                DailybillsCount:DailybillsCount,
                DailyProfitMoney:sum2,
                MonthlyBillsCount:MonthlybillsCount,
                MonthlyProfitMoney:sum3,
                balance:store.balance,
                totalBalance:store.totalBalance,
                monthlyRenewCount:sum4
            });
        } catch (err) {
            next(err);
        }
        
    },
    async countUser(req,res, next) {
        try {
            let {userId} = req.params
            let myUser = await checkExistThenGet(userId, User, { deleted: false });
            var mydate = new Date().toISOString().slice(0, 10);           
            let from = mydate + 'T00:00:00.000Z';
            let to= mydate + 'T23:59:00.000Z';
            const childCount = await User.count({deleted:false,parentUser:userId});
            const billsCount = await Bill.count({ deleted: false,user:userId/*,cancel:false*/});
            let totalPaidMoney = await Bill.find({ deleted: false,user:userId/*,cancel:false*/}).select('priceAfterDiscount');
            var sum = 0;
            for (var i = 0; i < totalPaidMoney.length; i++) { 
              sum += totalPaidMoney[i].priceAfterDiscount
            }
            let query = {
                $and: [
                    {createdAt: { $gte : new Date(from), $lte : new Date(to) }},
                    {deleted: false } 
                ]
            }
            const DailybillsCount = await Bill.count(query);
            let query2 = {
                $and: [
                    {createdAt: { $gte : new Date(from), $lte : new Date(to) }},
                    {deleted: false } ,
                    {user:userId},
                    //{cancel:false}
                ]
            }
            let DailyPaidMoney = await Bill.find(query2).select('priceAfterDiscount');
            var sum2 = 0;
            for (var i = 0; i < DailyPaidMoney.length; i++) { 
              sum2 += DailyPaidMoney[i].priceAfterDiscount
            }
            let date = new Date()
            let newDate = date.toISOString().slice(0, 8)  
            let firstDay = newDate + '01T00:00:00.000Z';
            //get days in month
            function daysInMonth (month, year) {
                return new Date(year, month , 0).getDate();
            }

            let DaysInCurrentMonth = daysInMonth(date.getMonth() + 1,date.getFullYear()); //number of day in month
            //last day
            let lastDay1= newDate + DaysInCurrentMonth+'T23:59:00.000Z';
            console.log(lastDay1)
            //get month name
            //get days in month
            function theMonth (month, year) {
                const datee = new Date(year, month , 0); 
                return datee.getMonth()
            }
            let month1 = theMonth(date.getMonth()+2,date.getFullYear())
            if(month1 == 0){
                month1 = 12
            }
            console.log(month1)
            let firstDay1 = date.getFullYear()+'-'+ month1 + '-01T00:00:00.000Z';
            if(month1 > 0 && month1 < 10){
                firstDay1 = date.getFullYear()+'-0'+ month1 + '-01T00:00:00.000Z';
            }
            let query3 = {
                $and: [
                    {createdAt: { $gte : new Date(firstDay1), $lte : new Date(lastDay1)}},
                    {deleted: false } ,
                    {user:userId},
                    //{cancel:false}
                ]
            }
            const MonthlybillsCount = await Bill.count(query3);
            let query4 = {
                $and: [
                    {createdAt: { $gte : new Date(firstDay1), $lte : new Date(lastDay1) }},
                    {deleted: false } ,
                    {user:userId},
                    //{cancel:false}
                ]
            }
            let MonthlyPaidMoney = await Bill.find(query4).select('priceAfterDiscount');
            var sum3 = 0;
            for (var i = 0; i < MonthlyPaidMoney.length; i++) { 
              sum3 += MonthlyPaidMoney[i].priceAfterDiscount
            }
            let sum4 = 0
            let activeChildCount = await User.find({deleted:false,parentUser:userId})
            .populate('package');
            activeChildCount.forEach(child => {
                console.log(child)
                if(child.package.defaultPackage != true &&  Date.parse(lastDay1) > Date.parse(child.packageStart) > Date.parse(firstDay1) ){
                    sum4 = sum4 + 1
                }
            });
           
            res.status(200).send({
                ChildCount:childCount,
                BillsCount:billsCount,
                totalPaidMoney:sum,
                DailybillsCount:DailybillsCount,
                DailyPaidMoney:sum2,
                MonthlyBillsCount:MonthlybillsCount,
                MonthlyPaidMoney:sum3,
                balance:myUser.balance,
                totalBalance:myUser.totalBalance,
                points:myUser.points,
                monthlyRenewCount:sum4
            });
        } catch (err) {
            next(err);
        }
        
    },

    async graph(req,res, next) {
        try {
            var date = new Date();
            
            //first day
            let newDate = date.toISOString().slice(0, 8)  
            let firstDay = newDate + '01T00:00:00.000Z';
            //get days in month
            function daysInMonth (month, year) {
                return new Date(year, month , 0).getDate();
            }

            let DaysInCurrentMonth = daysInMonth(date.getMonth() + 1,date.getFullYear()); //number of day in month
            //last day
            let lastDay1= newDate + DaysInCurrentMonth+'T23:59:00.000Z';
            console.log(lastDay1)
            //get month name
            //get days in month
            function theMonth (month, year) {
                const datee = new Date(year, month , 0); 
                return datee.getMonth()
            }
            let month1 = theMonth(date.getMonth()+2,date.getFullYear())
            if(month1 == 0){
                month1 = 12
            }
            console.log(month1)
            let firstDay1 = date.getFullYear()+'-'+ month1 + '-01T00:00:00.000Z';
            if(month1 > 0 && month1 < 10){
                firstDay1 = date.getFullYear()+'-0'+ month1 + '-01T00:00:00.000Z';
            }
            
            console.log(firstDay1)
            let query1 = {
                $and: [
                    {createdAt: { $gte : new Date(firstDay1), $lte : new Date(lastDay1) }},
                    {deleted: false } ,
                    
                ]
            };
            const userCount1 = await User.count(query1)
            //Bill count
            let Bills1 = await Bill.find(query1)
            var sum = 0;
            for (var i = 0; i < Bills1.length; i++) { 
              sum += Bills1[i].priceAfterDiscount
            }
            let BillsCount1 = await Bill.count(query1)

            //previous month
            let DaysInPreviousMonth = daysInMonth(date.getMonth(),date.getFullYear()); 

            //month name
            let month2 = theMonth(date.getMonth()+1,date.getFullYear())
            if(month2 == 0){
                month2 = 12
            }
            let firstDay2 = date.getFullYear()+'-'+ month2 + '-01T00:00:00.000Z';
            if(month2 > 0 && month2 < 10){
                firstDay2 = date.getFullYear()+'-0'+ month2 + '-01T00:00:00.000Z';
            }
            let lastDay2 = date.getFullYear()+'-'+ month2 +'-'+ DaysInPreviousMonth +'T23:59:00.000Z';
            if(month2 > 0 && month2 < 10){
            lastDay2 = date.getFullYear()+'-0'+ month2 +'-'+ DaysInPreviousMonth +'T23:59:00.000Z';
            }
            // user count
            let query2 = {
                $and: [
                    {createdAt: { $gte : new Date(firstDay2), $lte : new Date(lastDay2) }},
                    {deleted: false } ,
                ]
            };
            const userCount2 = await User.count(query2)

            let Bills2 = await Bill.find(query2)
            var sum2 = 0;
            for (var i = 0; i < Bills2.length; i++) { 
              sum2 += Bills2[i].priceAfterDiscount
            }
            let BillsCount2 =  await Bill.count(query2)

            // month -2
            let DaysInPreviousMonth2 = daysInMonth(date.getMonth()-1,date.getFullYear()); 
            //month name
            let month3 = theMonth(date.getMonth(),date.getFullYear())
            if(month3 == 0){
                month3 = 12
            }

            let firstDay3 = date.getFullYear()+'-'+ month3 + '-01T00:00:00.000Z';
            if(month3 > 0 && month3 < 10){
                firstDay3 = date.getFullYear()+'-0'+ month3 + '-01T00:00:00.000Z';
            }
            let lastDay3 = date.getFullYear()+'-'+ month3 +'-'+ DaysInPreviousMonth2 +'T23:59:00.000Z';
            if(month3 > 0 && month3 < 10){
            lastDay3 = date.getFullYear()+'-0'+ month3 +'-'+ DaysInPreviousMonth2 +'T23:59:00.000Z';
            }
            // user count
            let query3 = {
                $and: [
                    {createdAt: { $gte : new Date(firstDay3), $lte : new Date(lastDay3) }},
                    {deleted: false } ,
                ]
            };
            const userCount3 = await User.count(query3)

            //Bill count
           
            let Bills3 = await Bill.find(query3)
            var sum3 = 0;
            for (var i = 0; i < Bills3.length; i++) { 
            sum3 += Bills3[i].priceAfterDiscount
            }
            let BillsCount3 =await  Bill.count(query3)
            //month -3
            let DaysInPreviousMonth3 = daysInMonth(date.getMonth() -2,date.getFullYear()); 
            //month name
            let month4 = theMonth(date.getMonth()-1,date.getFullYear())
            if(month4 == 0){
                month4 = 12
            }
            let firstDay4 = date.getFullYear()+'-'+ month4 + '-01T00:00:00.000Z';
            if(month4 > 0 && month4 < 10){
                firstDay4 = date.getFullYear()+'-0'+ month4+ '-01T00:00:00.000Z';
            }
            let lastDay4 = date.getFullYear()+'-'+ month4 +'-'+ DaysInPreviousMonth3 +'T23:59:00.000Z';
            if(month4 > 0 && month4 < 10){
            lastDay4 = date.getFullYear()+'-0'+ month4 +'-'+ DaysInPreviousMonth3 +'T23:59:00.000Z';
            }
            // user count
            let query4 = {
                $and: [
                    {createdAt: { $gte : new Date(firstDay4), $lte : new Date(lastDay4) }},
                    {deleted: false } ,
                ]
            };
            const userCount4 = await User.count(query4)

            //Bill count
           
            let Bills4 = await Bill.find(query4)
            var sum4 = 0;
            for (var i = 0; i < Bills4.length; i++) { 
            sum4 += Bills4[i].priceAfterDiscount
            }
            let BillsCount4 = await Bill.count(query4)
             //month -4
            let DaysInPreviousMonth4 = daysInMonth(date.getMonth()-3,date.getFullYear()); 
            //month name
            let month5 = theMonth(date.getMonth()-2,date.getFullYear())
            if(month5 == 0){
                month5 = 12
            }
            let firstDay5 = date.getFullYear()+'-'+ month5 + '-01T00:00:00.000Z';
            if(month5 > 0 && month5 < 10){
                firstDay5 = date.getFullYear()+'-0'+ month5 + '-01T00:00:00.000Z';
            }
            let lastDay5 = date.getFullYear()+'-'+ month5 +'-'+ DaysInPreviousMonth4 +'T23:59:00.000Z';
            if(month5 > 0 && month5 < 10){
            lastDay5 = date.getFullYear()+'-0'+ month5 +'-'+ DaysInPreviousMonth4 +'T23:59:00.000Z';
            }
            // user count
            let query5 = {
                $and: [
                    {createdAt: { $gte : new Date(firstDay5), $lte : new Date(lastDay5) }},
                    {deleted: false } ,
                ]
            };
            const userCount5 = await User.count(query5)

            //Bill count
           
            let Bills5 = await Bill.find(query5)
            var sum5 = 0;
            for (var i = 0; i < Bills5.length; i++) { 
            sum5 += Bills5[i].priceAfterDiscount
            }
            let BillsCount5 = await Bill.count(query5)
            //month -5
            let DaysInPreviousMonth5 = daysInMonth(date.getMonth()-4,date.getFullYear()); 
            //month name
            let month6 = theMonth(date.getMonth()-3,date.getFullYear())
            if(month6 == 0){
                month6 = 12
            }
            let firstDay6 = date.getFullYear()+'-'+ month6 + '-01T00:00:00.000Z';
            if(month6 > 0 && month6 < 10){
                firstDay6 = date.getFullYear()+'-0'+ month6 + '-01T00:00:00.000Z';
            }
            let lastDay6 = date.getFullYear()+'-'+ month6 +'-'+ DaysInPreviousMonth5 +'T23:59:00.000Z';
            if(month6 > 0 && month6 < 10){
            lastDay6 = date.getFullYear()+'-0'+ month6 +'-'+ DaysInPreviousMonth5 +'T23:59:00.000Z';
            }
            // user count
            let query6 = {
                $and: [
                    {createdAt: { $gte : new Date(firstDay6), $lte : new Date(lastDay6) }},
                    {deleted: false } ,
                ]
            };
            const userCount6 = await User.count(query6)

            //Bill count
           
            let Bills6 = await Bill.find(query6)
            var sum6 = 0;
            for (var i = 0; i < Bills6.length; i++) { 
            sum6 += Bills6[i].priceAfterDiscount
            }
            let BillsCount6 = await Bill.count(query6)
            let user = [userCount1,userCount2,userCount3,userCount4,userCount5,userCount6];
            let BillsCount = [BillsCount1,BillsCount2,BillsCount3,BillsCount4,BillsCount5,BillsCount6]
            let BillTotal = [sum,sum2,sum3,sum4,sum5,sum6]
           
            res.status(200).send({
                user:user,
                BillsCount:BillsCount,
                BillTotal:BillTotal,
                months:[month1,month2,month3,month4,month5,month6]
            });
        } catch (err) {
            next(err);
        }
        
    },
    async graphStore(req,res, next) {
        try {
            var date = new Date();
            let {storeId} = req.params
            //first day
            let newDate = date.toISOString().slice(0, 8)  
            let firstDay = newDate + '01T00:00:00.000Z';
            //get days in month
            function daysInMonth (month, year) {
                return new Date(year, month , 0).getDate();
            }

            let DaysInCurrentMonth = daysInMonth(date.getMonth() + 1,date.getFullYear()); //number of day in month
            //last day
            let lastDay1= newDate + DaysInCurrentMonth+'T23:59:00.000Z';
            console.log(lastDay1)
            //get month name
            //get days in month
            function theMonth (month, year) {
                const datee = new Date(year, month , 0); 
                return datee.getMonth()
            }
            let month1 = theMonth(date.getMonth()+2,date.getFullYear())
            if(month1 == 0){
                month1 = 12
            }
            console.log(month1)
            let firstDay1 = date.getFullYear()+'-'+ month1 + '-01T00:00:00.000Z';
            if(month1 > 0 && month1 < 10){
                firstDay1 = date.getFullYear()+'-0'+ month1 + '-01T00:00:00.000Z';
            }
            
            console.log(firstDay1)
            let query1 = {
                $and: [
                    {createdAt: { $gte : new Date(firstDay1), $lte : new Date(lastDay1) }},
                    {deleted: false } ,
                    {parentUser:storeId}
                ]
            };
            const userCount1 = await User.count(query1)
            //Bill count
            let query11 = {
                $and: [
                    {createdAt: { $gte : new Date(firstDay1), $lte : new Date(lastDay1) }},
                    {deleted: false } ,
                    {store:storeId}
                ]
            };
            let Bills1 = await Bill.find(query11)
            var sum = 0;
            for (var i = 0; i < Bills1.length; i++) { 
              sum += Bills1[i].priceAfterDiscount
            }
            let BillsCount1 = await Bill.count(query11)

            //previous month
            let DaysInPreviousMonth = daysInMonth(date.getMonth(),date.getFullYear()); 

            //month name
            let month2 = theMonth(date.getMonth()+1,date.getFullYear())
            if(month2 == 0){
                month2 = 12
            }
            let firstDay2 = date.getFullYear()+'-'+ month2 + '-01T00:00:00.000Z';
            if(month2 > 0 && month2 < 10){
                firstDay2 = date.getFullYear()+'-0'+ month2 + '-01T00:00:00.000Z';
            }
            let lastDay2 = date.getFullYear()+'-'+ month2 +'-'+ DaysInPreviousMonth +'T23:59:00.000Z';
            if(month2 > 0 && month2 < 10){
            lastDay2 = date.getFullYear()+'-0'+ month2 +'-'+ DaysInPreviousMonth +'T23:59:00.000Z';
            }
            // user count
            let query2 = {
                $and: [
                    {createdAt: { $gte : new Date(firstDay2), $lte : new Date(lastDay2) }},
                    {deleted: false } ,
                    {parentUser:storeId}
                ]
            };
            const userCount2 = await User.count(query2)
            let query22 = {
                $and: [
                    {createdAt: { $gte : new Date(firstDay2), $lte : new Date(lastDay2) }},
                    {deleted: false } ,
                    {store:storeId}
                ]
            };
            let Bills2 = await Bill.find(query22)
            var sum2 = 0;
            for (var i = 0; i < Bills2.length; i++) { 
              sum2 += Bills2[i].priceAfterDiscount
            }
            let BillsCount2 =  await Bill.count(query22)

            // month -2
            let DaysInPreviousMonth2 = daysInMonth(date.getMonth()-1,date.getFullYear()); 
            //month name
            let month3 = theMonth(date.getMonth(),date.getFullYear())
            if(month3 == 0){
                month3 = 12
            }

            let firstDay3 = date.getFullYear()+'-'+ month3 + '-01T00:00:00.000Z';
            if(month3 > 0 && month3 < 10){
                firstDay3 = date.getFullYear()+'-0'+ month3 + '-01T00:00:00.000Z';
            }
            let lastDay3 = date.getFullYear()+'-'+ month3 +'-'+ DaysInPreviousMonth2 +'T23:59:00.000Z';
            if(month3 > 0 && month3 < 10){
            lastDay3 = date.getFullYear()+'-0'+ month3 +'-'+ DaysInPreviousMonth2 +'T23:59:00.000Z';
            }
            // user count
            let query3 = {
                $and: [
                    {createdAt: { $gte : new Date(firstDay3), $lte : new Date(lastDay3) }},
                    {deleted: false } ,
                ]
            };
            const userCount3 = await User.count(query3)

            //Bill count
            let query33 = {
                $and: [
                    {createdAt: { $gte : new Date(firstDay3), $lte : new Date(lastDay3) }},
                    {deleted: false } ,
                    {store:storeId}
                ]
            };
            let Bills3 = await Bill.find(query33)
            var sum3 = 0;
            for (var i = 0; i < Bills3.length; i++) { 
            sum3 += Bills3[i].priceAfterDiscount
            }
            let BillsCount3 =await  Bill.count(query33)
            //month -3
            let DaysInPreviousMonth3 = daysInMonth(date.getMonth() -2,date.getFullYear()); 
            //month name
            let month4 = theMonth(date.getMonth()-1,date.getFullYear())
            if(month4 == 0){
                month4 = 12
            }
            let firstDay4 = date.getFullYear()+'-'+ month4 + '-01T00:00:00.000Z';
            if(month4 > 0 && month4 < 10){
                firstDay4 = date.getFullYear()+'-0'+ month4+ '-01T00:00:00.000Z';
            }
            let lastDay4 = date.getFullYear()+'-'+ month4 +'-'+ DaysInPreviousMonth3 +'T23:59:00.000Z';
            if(month4 > 0 && month4 < 10){
            lastDay4 = date.getFullYear()+'-0'+ month4 +'-'+ DaysInPreviousMonth3 +'T23:59:00.000Z';
            }
            // user count
            let query4 = {
                $and: [
                    {createdAt: { $gte : new Date(firstDay4), $lte : new Date(lastDay4) }},
                    {deleted: false } ,
                    {parentUser:storeId}
                ]
            };
            const userCount4 = await User.count(query4)

            //Bill count
            let query44 = {
                $and: [
                    {createdAt: { $gte : new Date(firstDay4), $lte : new Date(lastDay4) }},
                    {deleted: false } ,
                    {store:storeId}
                ]
            };
            let Bills4 = await Bill.find(query44)
            var sum4 = 0;
            for (var i = 0; i < Bills4.length; i++) { 
            sum4 += Bills4[i].priceAfterDiscount
            }
            let BillsCount4 = await Bill.count(query44)
             //month -4
            let DaysInPreviousMonth4 = daysInMonth(date.getMonth()-3,date.getFullYear()); 
            //month name
            let month5 = theMonth(date.getMonth()-2,date.getFullYear())
            if(month5 == 0){
                month5 = 12
            }
            let firstDay5 = date.getFullYear()+'-'+ month5 + '-01T00:00:00.000Z';
            if(month5 > 0 && month5 < 10){
                firstDay5 = date.getFullYear()+'-0'+ month5 + '-01T00:00:00.000Z';
            }
            let lastDay5 = date.getFullYear()+'-'+ month5 +'-'+ DaysInPreviousMonth4 +'T23:59:00.000Z';
            if(month5 > 0 && month5 < 10){
            lastDay5 = date.getFullYear()+'-0'+ month5 +'-'+ DaysInPreviousMonth4 +'T23:59:00.000Z';
            }
            // user count
            let query5 = {
                $and: [
                    {createdAt: { $gte : new Date(firstDay5), $lte : new Date(lastDay5) }},
                    {deleted: false } ,
                    {parentUser:storeId}
                ]
            };
            const userCount5 = await User.count(query5)

            //Bill count
            let query55= {
                $and: [
                    {createdAt: { $gte : new Date(firstDay5), $lte : new Date(lastDay5) }},
                    {deleted: false } ,
                    {store:storeId}
                ]
            };
            let Bills5 = await Bill.find(query55)
            var sum5 = 0;
            for (var i = 0; i < Bills5.length; i++) { 
            sum5 += Bills5[i].priceAfterDiscount
            }
            let BillsCount5 = await Bill.count(query55)
            //month -5
            let DaysInPreviousMonth5 = daysInMonth(date.getMonth()-4,date.getFullYear()); 
            //month name
            let month6 = theMonth(date.getMonth()-3,date.getFullYear())
            if(month6 == 0){
                month6 = 12
            }
            let firstDay6 = date.getFullYear()+'-'+ month6 + '-01T00:00:00.000Z';
            if(month6 > 0 && month6 < 10){
                firstDay6 = date.getFullYear()+'-0'+ month6 + '-01T00:00:00.000Z';
            }
            let lastDay6 = date.getFullYear()+'-'+ month6 +'-'+ DaysInPreviousMonth5 +'T23:59:00.000Z';
            if(month6 > 0 && month6 < 10){
            lastDay6 = date.getFullYear()+'-0'+ month6 +'-'+ DaysInPreviousMonth5 +'T23:59:00.000Z';
            }
            // user count
            let query6 = {
                $and: [
                    {createdAt: { $gte : new Date(firstDay6), $lte : new Date(lastDay6) }},
                    {deleted: false } ,
                    {parentUser:storeId}
                ]
            };
            const userCount6 = await User.count(query6)

            //Bill count
            let query66 = {
                $and: [
                    {createdAt: { $gte : new Date(firstDay6), $lte : new Date(lastDay6) }},
                    {deleted: false } ,
                    {store:storeId}
                ]
            };
            let Bills6 = await Bill.find(query66)
            var sum6 = 0;
            for (var i = 0; i < Bills6.length; i++) { 
            sum6 += Bills6[i].priceAfterDiscount
            }
            let BillsCount6 = await Bill.count(query66)
            let user = [userCount1,userCount2,userCount3,userCount4,userCount5,userCount6];
            let BillsCount = [BillsCount1,BillsCount2,BillsCount3,BillsCount4,BillsCount5,BillsCount6]
            let BillTotal = [sum,sum2,sum3,sum4,sum5,sum6]
           
            res.status(200).send({
                user:user,
                BillsCount:BillsCount,
                BillTotal:BillTotal,
                months:[month1,month2,month3,month4,month5,month6]
            });
        } catch (err) {
            next(err);
        }
        
    }
    
}