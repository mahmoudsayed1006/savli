import { checkExist, checkExistThenGet } from "../../helpers/CheckMethods";
import ApiResponse from "../../helpers/ApiResponse";
import User from "../../models/user/user.model";
import Product from "../../models/product/product.model";
import Cart from "../../models/cart/cart.model";
import ApiError from '../../helpers/ApiError';
import Notif from "../../models/notif/notif.model";
import { sendNotifiAndPushNotifi } from "../../services/notification-service";
const populateQuery = [
    
    { path: 'user', model: 'user' },
    {
        path: 'product', model: 'product' ,
        populate: { path: 'category', model: 'category' },
       
    },
];

export default {
    async findAll(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let { userId } = req.params;
            let query = { user: userId,deleted:false };
            let Carts = await Cart.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const CartsCount = await Cart.count(query);
            const pageCount = Math.ceil(CartsCount / limit);

            res.send(new ApiResponse(Carts, page, pageCount, limit, CartsCount, req));
        } catch (err) {
            next(err);
        }
    },
    async create(req, res, next) {
        try {
            let {productId} = req.params;
            let user = await checkExistThenGet(req.user._id, User);
            let arr = user.carts;
            let product = await checkExistThenGet(productId, Product);
            var found = arr.find(function(element) {
                return element == productId;
            });
            if(!found){
                user.carts.push(productId);
                let cart =  await Cart.create({ user: req.user._id, product: productId});
            }
            await user.save();
            
 
            res.status(201).send({
                user: await User.findById(req.user._id),
                product:await checkExistThenGet(productId, Product)
            });
        } catch (error) {
            next(error)
        }
    },
    async unCart(req, res, next) {
        try {
            let {productId } = req.params;
            let cart = await Cart.findOne({ user: req.user._id, product: productId,deleted:false})
            console.log(cart)
            if(cart == null)
                return next(new ApiError(403, ('removed before')));
            let Carts = await checkExistThenGet(cart.id, Cart, { deleted: false });
            if (Carts.user != req.user._id)
                return next(new ApiError(403, ('not allowed')));
                Carts.deleted = true;
            await Carts.save();
            let user = await checkExistThenGet(req.user._id, User);

            let arr = user.carts;
            console.log(arr);
            for(let i = 0;i<= arr.length;i=i+1){
                if(arr[i] == productId){
                    arr.splice(i, 1);
                }
            }
            user.carts = arr;
            await user.save();
            res.send({user: await User.findById(req.user._id)});
        } catch (error) {
            next(error)
        }
    },

}