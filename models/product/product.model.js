import mongoose, { Schema } from "mongoose";
import { isImgUrl } from "../../helpers/CheckMethods";
import autoIncrement from 'mongoose-auto-increment';
const ProductSchema=new Schema({
    _id: {
        type: Number,
        required: true
    },
    name_en: {
        type: String,
        required: true
    },
    name_ar: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    store:{
        type: Number,
        ref: 'user'
    },
    rule:{
        type: Number,
        ref: 'rule'
    },
    hasRule:{
        type:Boolean,
        default:false
    },
    img: [{
        type: String,
        required: true,
        
    }],
    properties: [new Schema({
        attr: {
            type: String,
        },
        value: {
            type: String
        },
        
    })],
    description:{
        type:String,
        required:true
    },
    available:{
        type:Boolean,
        default:true
    },
    deleted:{
        type:Boolean,
        default:false
    },

},{ timestamps: true });
ProductSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete  ret.numerator;
        delete  ret.denominator;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
ProductSchema.plugin(autoIncrement.plugin, { model: 'product', startAt: 1 });

export default mongoose.model('product', ProductSchema);