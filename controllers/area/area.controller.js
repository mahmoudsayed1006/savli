import Area from "../../models/area/area.model";
import City from "../../models/city/city.model";
import User from "../../models/user/user.model";
import Report from "../../models/reports/report.model";
import { body } from "express-validator/check";
import { checkValidations } from "../shared/shared.controller";
import ApiError from "../../helpers/ApiError";
import { checkExist ,checkExistThenGet} from "../../helpers/CheckMethods";
import ApiResponse from "../../helpers/ApiResponse";

export default {
    validateAreaBody() {
        return [
            body('areaName').not().isEmpty().withMessage('areaName Required'),
            body('arabicAreaName').not().isEmpty().withMessage('arabic area Name Required'),
            
        ];
    },
    async create(req, res, next) {
        try {
            let user = req.user, { cityId } = req.params;
            await checkExist(cityId, City);
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
            const validatedBody = checkValidations(req);
            validatedBody.city = cityId;
            let area = await Area.create({ ...validatedBody });
            let reports = {
                "action":"Create New Area",
            };
            let report = await Report.create({...reports, user: user });
            return res.status(201).send(area);
        } catch (error) {
            next(error);
            send(error);
        }
    },
    async getById(req, res, next) {
        try {
            let user = req.user;
            let { areaId } = req.params;

            if (user.type != 'ADMIN')
               return next(new ApiError(403, ('admin.auth')));
            await checkExist(areaId, Area, { deleted: false });

            let area = await Area.findById(areaId).populate('city');
            return res.send(area);
        } catch (error) {
            next(error);
        }
    },
    async update(req, res, next) {
        try {
            let user = req.user;
            let { areaId } = req.params;

            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
            const validatedBody = checkValidations(req);
            let area = await Area.findByIdAndUpdate(areaId, { ...validatedBody });

            let reports = {
                "action":"Update Area",
            };
            let report = await Report.create({...reports, user: user });
            return res.status(200).send(area);
        } catch (error) {
            next(error);
        }
    },

    async getAll(req, res, next) {
        try {
            let { cityId } = req.params;
            await checkExist(cityId, City, { deleted: false });
            let areas = await Area.find({ 'city': cityId, deleted: false });
            return res.send(areas);
        } catch (error) {
            next(error);
        }
    },

    async getAllPaginated(req, res, next) {
        try {
            let user = req.user;
            let { cityId } = req.params;
            if (user.type != 'ADMIN')
               return next(new ApiError(403, ('admin.auth')));

            let page = +req.query.page || 1, limit = +req.query.limit || 20;


            let areas = await Area.find({ 'city': cityId, deleted: false })
                .populate('city')
                .limit(limit)
                .skip((page - 1) * limit).sort({ _id: -1 });
            let count = await Area.count({ 'city': cityId, deleted: false });

            const pageCount = Math.ceil(count / limit);

            res.send(new ApiResponse(areas, page, pageCount, limit, count, req));
        } catch (error) {
            next(error);
        }
    },

    async delete(req, res, next) {
        let { areaId } = req.params;
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
               return next(new ApiError(403, ('admin.auth')));
            let area = await checkExistThenGet(areaId, Area);
            
            area.deleted = true;
            await area.save();
            let reports = {
                "action":"Delete Area",
            };
            let report = await Report.create({...reports, user: user });
            res.send('delete success');

        } catch (err) {
            next(err);
        }
    },


}