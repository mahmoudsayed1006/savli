import mongoose, { Schema } from "mongoose";
import autoIncrement from 'mongoose-auto-increment';
const offerPackageSchema=new Schema({
    _id: {
        type: Number,
        required: true
    },
    points: {
        type: Number,
        required: true,
    },
    description: {
        type: String,
        default:""
    },
    spacial:{
        type:Boolean,
        default:false
    },
    deleted:{
        type:Boolean,
        default:false
    },

},{ timestamps: true });
offerPackageSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
offerPackageSchema.plugin(autoIncrement.plugin, { model: 'offerPackage', startAt: 1 });

export default mongoose.model('offerPackage', offerPackageSchema);