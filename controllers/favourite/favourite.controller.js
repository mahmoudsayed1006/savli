import { checkExist, checkExistThenGet } from "../../helpers/CheckMethods";
import ApiResponse from "../../helpers/ApiResponse";
import User from "../../models/user/user.model";
import Favourite from "../../models/favourite/favourite.model";
import ApiError from '../../helpers/ApiError';
import Notif from "../../models/notif/notif.model";
import { sendNotifiAndPushNotifi } from "../../services/notification-service";
const populateQuery = [
    {
        path: 'favPerson', model: 'user',
        populate: { path: 'category', model: 'category' },
    },
    {
        path: 'favPerson', model: 'user',
        populate: { path: 'subCategory', model: 'category' },
    },
    {
        path: 'favPerson', model: 'user',
        populate: { path: 'city', model: 'city' },
    },
    {
        path: 'favPerson', model: 'user',
        populate: { path: 'area', model: 'area' },
    },
    {path: 'user', model: 'user' },
   
];
export default {
    async findAll(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let { userId } = req.params;
            let query = { user: userId,deleted:false };
            let favourites = await Favourite.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const favouritesCount = await Favourite.count(query);
            const pageCount = Math.ceil(favouritesCount / limit);

            res.send(new ApiResponse(favourites, page, pageCount, limit, favouritesCount, req));
        } catch (err) {
            next(err);
        }
    },
    async create(req, res, next) {
        try {
            let {favPerson} = req.params;
            let user = await checkExistThenGet(req.user._id, User);
            let arr = user.favourite;
            var found = arr.find(function(element) {
                return element == favPerson;
            });
            if(!found){
                user.favourite.push(favPerson);
                await user.save();
                let favPersonUser = await checkExistThenGet(favPerson, User);
                let favourite =  await Favourite.create({ user: req.user._id, favPerson: favPerson });
                if(favPersonUser.notif == true){
                    sendNotifiAndPushNotifi({
                        targetUser: favPerson, 
                        fromUser: req.user, 
                        text: 'new notification',
                        subject: favourite.id,
                        subjectType: req.user.username + ' add you to favourite list'
                    });
                    let notif = {
                        "description":req.user.username + ' add you to favourite list',
                        "arabicDescription":'باضافتك الى قائمه اهتماماته'+ req.user.username +' قام'
                    }
                    await Notif.create({...notif,resource:req.user,target:favPerson,favourite:favourite.id});
                }
            } else{
                return next(new ApiError(403, ('this user is found in your list')));
            }
            
            
            res.status(201).send({user});
        } catch (error) {
            next(error)
        }
    },
    async unFavourite(req, res, next) {
        try {
            let {favPerson } = req.params;
            let favourite = await Favourite.findOne({ user: req.user._id, favPerson: favPerson,deleted:false})
            if(!await Favourite.findOne({ user: req.user._id, favPerson: favPerson,deleted:false})){
                return next(new ApiError(403, ('this user is found in your list')));
            }
            let favourites = await checkExistThenGet(favourite.id, Favourite, { deleted: false });
            if (favourites.user != req.user._id)
                return next(new ApiError(403, ('not allowed')));
                favourites.deleted = true;
            await favourites.save();
            let user = await checkExistThenGet(req.user._id, User);

            let arr = user.favourite;
            console.log(arr);
            for(let i = 0;i<= arr.length;i=i+1){
                if(arr[i] == favPerson){
                    arr.splice(i, 1);
                }
            }
            user.favourite = arr;
            await user.save();
            res.send({user});
        } catch (error) {
            next(error)
        }
    },

}