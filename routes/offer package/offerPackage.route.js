import express from 'express';
import offerPackageController from '../../controllers/offer package/offerPackage.controller';
import { requireAuth } from '../../services/passport';

const router = express.Router();

router.route('/')
    .post(
        requireAuth,
        offerPackageController.validateBody(),
        offerPackageController.create
    )
    .get(offerPackageController.findAll);

router.route('/withoutPagenation/get')
    .get(offerPackageController.getAll);
 
router.route('/:packageId')
    .put(
        requireAuth,
        offerPackageController.validateBody(true),
        offerPackageController.update
    )
    .get(offerPackageController.findById)
    .delete( requireAuth,offerPackageController.delete);


export default router;