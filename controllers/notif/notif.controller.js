import User from "../../models/user/user.model";
import { checkExistThenGet, checkExist} from "../../helpers/CheckMethods";
import Notif from "../../models/notif/notif.model";
import ApiResponse from "../../helpers/ApiResponse";
const populateQuery = [
    {
        path: 'rate', model: 'rate',
        populate: { path: 'user', model: 'user' },
    },
    {
        path: 'rate', model: 'rate',
        populate: { path: 'store', model: 'user' },
    },
    {
        path: 'bill', model: 'bill',
        populate: { path: 'user', model: 'user' },
    },
    {
        path: 'bill', model: 'bill',
        populate: { path: 'store', model: 'user' },
    },
    {
        path: 'bill.store', model: 'user',
        populate: { path: 'city', model: 'city' },
    },
    {
        path: 'bill.store', model: 'user',
        populate: { path: 'area', model: 'area' },
    },
    
    {
        path: 'favourite', model: 'favourite',
        populate: { path: 'user', model: 'user' },
    },
    {
        path: 'favourite', model: 'favourite',
        populate: { path: 'favPerson', model: 'user' },
    },
    
    {
        path: 'resource', model: 'user',
        populate: { path: 'city', model: 'city' },
    },
    {
        path: 'resource', model: 'user',
        populate: { path: 'area', model: 'area' },
    },
    {
        path: 'resource', model: 'user',
        populate: { path: 'category', model: 'category' },
    },
    {
        path: 'resource', model: 'user',
        populate: { path: 'subCategory', model: 'category' },
    },
    {
        path: 'target', model: 'user',
        populate: { path: 'city', model: 'city' },
    },
    {
        path: 'target', model: 'user',
        populate: { path: 'area', model: 'area' },
    },
    
];
export default {
    async find(req, res, next) {
        try {
            let user = req.user._id;
            await checkExist(req.user._id, User);
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let query = { deleted: false,target:user };
            let notifs = await Notif.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);
            const notifsCount = await Notif.count(query);
            const pageCount = Math.ceil(notifsCount / limit);

            res.send(new ApiResponse(notifs, page, pageCount, limit, notifsCount, req));
        } catch (err) {
            next(err);
        }
    },
    async read(req, res, next) {
        try {
            let { notifId} = req.params;
            let notif = await checkExistThenGet(notifId, Notif);
            notif.read = true;
            await notif.save();
            res.send('notif read');
        } catch (error) {
            next(error);
        }
    },

    async unread(req, res, next) {
        try {
            let { notifId} = req.params;
            let notif = await checkExistThenGet(notifId, Notif);
            notif.read = false;
            await notif.save();
            res.send('notif unread');
        } catch (error) {
            next(error);
        }
    },
    async delete(req, res, next) {
        try {
            let { notifId} = req.params;
            let notif = await checkExistThenGet(notifId, Notif);
            notif.deleted = true;
            await notif.save();
            res.send('notif deleted');
        } catch (error) {
            next(error);
        }
    },
    async unreadCount(req, res, next) {
        try {
            let user = req.user._id;
            await checkExist(req.user._id, User);
            let query = { deleted: false,target:user,read:false };
            const unreadCount = await Notif.count(query);
            res.status(200).send({
                unread:unreadCount,
            });
        } catch (err) {
            next(err);
        }
    },
}