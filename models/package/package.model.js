import mongoose, { Schema } from "mongoose";
import autoIncrement from 'mongoose-auto-increment';
const PackageSchema=new Schema({
    _id: {
        type: Number,
        required: true
    },
    month:{
        type:Number,
        required:true
    },
    cost:{
        type:String,
        required:true
    },
    freeOperations:{
        type:Number,
        
    },
    ratioForParentStore: {
        type: Number,
        required: true,
        default:10
    },
    ratioForParentUser: {
        type: Number,
        required: true,
        default:10
    },
    pointsForStore: {
        type: Number,
        required: true,
    },
    pointsForUser: {
        type: Number,
        required: true,
    },
    defaultPackage:{
        type:Boolean,
        default:false
    },
    deleted:{
        type:Boolean,
        default:false
    },

},{ timestamps: true });
PackageSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
PackageSchema.plugin(autoIncrement.plugin, { model: 'package', startAt: 1 });

export default mongoose.model('package', PackageSchema);