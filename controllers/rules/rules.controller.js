import ApiResponse from "../../helpers/ApiResponse";
import Report from "../../models/reports/report.model";
import ApiError from '../../helpers/ApiError';
import { checkExist, checkExistThenGet} from "../../helpers/CheckMethods";
import { checkValidations } from "../shared/shared.controller";
import { body } from "express-validator/check";
import Rule from "../../models/rules/rules.model";
import User from "../../models/user/user.model";

const populateQuery = [
    {path: 'store', model: 'user'},
    {path: 'package', model: 'package'},
]
export default {

    async findAll(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20 
            ,{store,thePackage} = req.query;

            let query = {deleted: false };
            if(store)
                query.store = store
            if(thePackage)
                query.package = thePackage
            let rules = await Rule.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const rulesCount = await Rule.count(query);
            const pageCount = Math.ceil(rulesCount / limit);

            res.send(new ApiResponse(rules, page, pageCount, limit, rulesCount, req));
        } catch (err) {
            next(err);
        }
    },
    async getAll(req, res, next) {

        try {
            let {store,thePackage} = req.query;
            let query = {deleted: false };
            if(store)
                query.store = store
            if(thePackage)
                query.package = thePackage
            let rules = await Rule.find(query).populate(populateQuery)
           
            res.send(rules);
        } catch (err) {
            next(err);
        }
    },
   
    validateBody(isUpdate = false) {
        let validations = [
            body('description').not().isEmpty().withMessage('description is required'),
            body('store').not().isEmpty().withMessage('store is required')
                .isNumeric().withMessage('store should be number'),
            body('package').not().isEmpty().withMessage('package is required')
                .isNumeric().withMessage('package should be number'),
            body('discount').not().isEmpty().withMessage('discount is required')
                .isNumeric().withMessage('discount should be number'),
            
        ];
        return validations;
    },

    async create(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin only')));
    
            const validatedBody = checkValidations(req);
            let createdrule = await Rule.create({ ...validatedBody});
            let store = await checkExistThenGet(validatedBody.store, User, { deleted: false });
            let arr = store.rules?store.rules:[];
            var found = arr.find(function(element) {
                return element == createdrule.id;
            });
            if(!found){
                arr.push(createdrule.id);
                store.rules = arr
                await store.save();
            }
            let reports = {
                "action":"Create rule",
            };
            let report = await Report.create({...reports, user: user });
            
            res.status(201).send(createdrule);
        } catch (err) {
            next(err);
        }
    },


    async findById(req, res, next) {
        try {
            let { ruleId } = req.params;
            await checkExist(ruleId, Rule, { deleted: false });
            let rules = await Rule.findById(ruleId);
            res.send(rules);
        } catch (err) {
            next(err);
        }
    },
    async update(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
            return next(new ApiError(403, ('admin only')));

            let { ruleId } = req.params;
            await checkExist(ruleId, Rule, { deleted: false });

            const validatedBody = checkValidations(req);
            let updatedrule = await Rule.findByIdAndUpdate(ruleId, {
                ...validatedBody,
            }, { new: true });
            let reports = {
                "action":"Update rule",
            };
            let report = await Report.create({...reports, user: user });
            res.status(200).send(updatedrule);
        }
        catch (err) {
            next(err);
        }
    },
    
    async delete(req, res, next) {
        try {
            let user = req.user;
            
            let { ruleId } = req.params;
            let rules = await checkExistThenGet(ruleId, Rule, { deleted: false });
            
            rules.deleted = true;
            await rules.save();
            let store = await checkExistThenGet(rules.store, User, { deleted: false });
            let arr = store.rules;
            for(let i = 0;i<= arr.length;i=i+1){
                if(arr[i] == ruleId){
                    arr.splice(i, 1);
                }
            }
            console.log(arr)
            store.rules = arr;
            await store.save()
            let reports = {
                "action":"Delete rule",
            };
            let report = await Report.create({...reports, user: user });
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },
};