import ApiResponse from "../../helpers/ApiResponse";
import Setting from "../../models/setting/setting.model";
import Report from "../../models/reports/report.model";
import ApiError from '../../helpers/ApiError';

import { checkExist, checkExistThenGet, isImgUrl } from "../../helpers/CheckMethods";
import { handleImg, checkValidations } from "../shared/shared.controller";
import { body } from "express-validator/check";

export default {

    async findAll(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let query = {deleted: false };
            let Settings = await Setting.find(query)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const SettingsCount = await Setting.count(query);
            const pageCount = Math.ceil(SettingsCount / limit);

            res.send(new ApiResponse(Settings, page, pageCount, limit, SettingsCount, req));
        } catch (err) {
            next(err);
        }
    },

    validateBody(isUpdate = false) {
        let validations = [
            body('kmCost').not().isEmpty().withMessage('kmCost is required'),
           
        ];
        return validations;
    },

    async create(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
    
            const validatedBody = checkValidations(req);
            let createdSetting = await Setting.create({ ...validatedBody});
            let reports = {
                "action":"Create Setting",
            };
            let report = await Report.create({...reports, user: user });
            res.status(201).send(createdSetting);
        } catch (err) {
            next(err);
        }
    },


    async findById(req, res, next) {
        try {
            let { SettingId } = req.params;
            await checkExist(settingId, Setting, { deleted: false });
            let setting = await Setting.findById(SettingId);
            res.send(setting);
        } catch (err) {
            next(err);
        }
    },
    async update(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { SettingId } = req.params;
            await checkExist(SettingId, Setting, { deleted: false });

            const validatedBody = checkValidations(req);
            let updatedSetting = await Setting.findByIdAndUpdate(SettingId, {
                ...validatedBody,
            }, { new: true });
            let reports = {
                "action":"Update Setting value",
            };
            let report = await Report.create({...reports, user: user });
            res.status(200).send(updatedSetting);
        }
        catch (err) {
            next(err);
        }
    },

    async delete(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
                
            let { SettingId } = req.params;
            let setting = await checkExistThenGet(SettingId, Setting, { deleted: false });
            setting.deleted = true;
            await setting.save();
            let reports = {
                "action":"Delete Setting",
            };
            let report = await Report.create({...reports, user: user });
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },
  
};