import mongoose, { Schema } from "mongoose";
import autoIncrement from 'mongoose-auto-increment';
const RuleSchema=new Schema({
    _id: {
        type: Number,
        required: true
    },
    description:{
        type:String,
        required:true
    },
    discount:{
        type:Number,
        required:true
    },
    package: {
        type: Number,
        required: true,
        ref:'package'
    },
    store: {
        type: Number,
        required: true,
        ref:'user'
    },
    deleted:{
        type:Boolean,
        default:false
    },

},{ timestamps: true });
RuleSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
RuleSchema.plugin(autoIncrement.plugin, { model: 'rule', startAt: 1 });

export default mongoose.model('rule', RuleSchema);