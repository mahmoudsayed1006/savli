import { checkExist, checkExistThenGet, isLng, isLat, isArray, isNumeric } from "../../helpers/CheckMethods";
import ApiResponse from "../../helpers/ApiResponse";
import Order from "../../models/order/order.model";
import { sendNotifiAndPushNotifi } from "../../services/notification-service";
import { body } from "express-validator/check";
import { ValidationError } from "mongoose";
import { handleImg, checkValidations ,distance} from "../shared/shared.controller";
import ApiError from "../../helpers/ApiError";
import User from "../../models/user/user.model";
import Report from "../../models/reports/report.model";
import Notif from "../../models/notif/notif.model"
import Setting from "../../models/setting/setting.model"
import Cart from "../../models/cart/cart.model";
import Package from "../../models/package/package.model";
import Rule from "../../models/rules/rules.model";
import Bill from "../../models/bills/bills.model";
import Product from "../../models/product/product.model";
const populateQuery = [
    { path: 'client', model: 'user' },
    { path: 'store', model: 'user' }, 
    { path: 'bill', model: 'bill' },  
    {
        path: 'productOrders.product', model: 'product',
        populate: { path: 'rule', model: 'rule' }
    },
];
function validatedestination(location) {
    if (!isLng(location[0]))
        throw new ValidationError.UnprocessableEntity({ keyword: 'location', message: 'location[0] is invalid lng' });
    if (!isLat(location[1]))
        throw new ValidationError.UnprocessableEntity({ keyword: 'location', message: 'location[1] is invalid lat' });
}

var OrderController = {
    async findOrders(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20
                ,{ status,client,store,accept,start,end} = req.query
                , query = {deleted: false };
            if(start && end) {
                let from = start + 'T00:00:00.000Z';
                let to= end + 'T23:59:00.000Z';
                console.log( from)
                query = { 
                    createdAt: { $gt : new Date(from), $lt : new Date(to) }
                };
            } 
            if (status)
                query.status = status;
           
            if (client){
                query.client = client;
            } 
            if (store){
                query.store = store;
            } 
            if (accept){
                query.accept = accept;
            } 
           
            let orders = await Order.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const ordersCount = await Order.count(query);
            const pageCount = Math.ceil(ordersCount / limit);

            res.send(new ApiResponse(orders, page, pageCount, limit, ordersCount, req));
        } catch (err) {
            next(err);
        }
    },

    validateCreated() {
        let validations = [
           body('clientDestination').not().isEmpty().withMessage('client Destination is required'),
           body('storeDestination').not().isEmpty().withMessage('target is required'),
           body('address').not().isEmpty().withMessage('address is required'),
           body('city').not().isEmpty().withMessage('city is required'),
           body('area').not().isEmpty().withMessage('area is required'),
           body('productOrders').custom(vals => isArray(vals)).withMessage('productOrders should be an array')
                .isLength({ min: 1 }).withMessage('productOrders should have at least one element of productOrder')
                .custom(async (productOrders, { req }) => {
                    let prevProductId;
                    for (let productOrder of productOrders) {
                        // check if it's duplicated product
                        if (prevProductId && prevProductId === productOrder.product)
                            throw new Error(`Duplicated Product : ${productOrder.product}`);
                        prevProductId = productOrder.product;
                        
                        // check if count is a valid number 
                        if (!isNumeric(productOrder.count))
                            throw new Error(`Product: ${productOrder.product} has invalid count: ${productOrder.count}!`);
                       
                    }
                    return true;
                }),

        ];
        return validations;
    },
    async create(req, res, next) {
        try {
            let user = req.user;
            await checkExist(req.user._id, User);
            let theUser = await checkExistThenGet(req.user._id, User, { deleted: false }); 
            let packagee = await checkExistThenGet(user.package, Package, { deleted: false });
            if(packagee.defaultPackage == true){
                if(user.freeOperationsNum >= packagee.freeOperations ){
                    return next(new ApiError(500, ('sorry this user finish all free operation')));
                }
            }
            const validatedBody = checkValidations(req);
            if (user.block == true)
            return next(new ApiError(403, ('blocked'))); 
            let myUser = await checkExistThenGet(req.user._id, User);
            myUser.carts = [];
            let carts = await Cart.find({ user: req.user._id });
            for (let cart of carts ) {
                cart.deleted = true;
                await cart.save();
            }
            await myUser.save();

            validatedBody.client = req.user._id;

           let orders = [];
            let products = validatedBody.productOrders;
            let storesIds = [];
            products.forEach(id => {
                storesIds.push(id.store)
            });  
            storesIds = [ ...new Set(storesIds) ];
            for(var i = 0;i<storesIds.length;i++) { 
                var filterObj = products.filter(function(e) {
                    return e.store == storesIds[i];
                  });
                orders.push(filterObj)
            }
            console.log(orders)
            for(var j =0;j<orders.length;j++){
                validatedBody.productOrders = orders[j];
                validatedBody.store = validatedBody.productOrders[0].store;
                let store = await checkExistThenGet(validatedBody.store, User);
                let total = 0;
                let totalAfterDiscount = 0
                for (let singleProduct of validatedBody.productOrders) {
                    let productDetail = await checkExistThenGet(singleProduct.product, Product);
                    total += productDetail.price * singleProduct.count;
                    if(productDetail.hasRule){
                        let rule = await checkExistThenGet(productDetail.rule, Rule, { deleted: false });
                        let discount = rule.discount;
                        let newPrice = productDetail.price - (productDetail.price * discount)/100;
                        totalAfterDiscount += newPrice * singleProduct.count;
                    }else{
                        let arr = store.discounts
                        let obj = arr.find(o => o.package == theUser.package);
                        let index = arr.indexOf(obj);
                        let discount = arr[index].discount;
                        console.log(discount)
                        let newPrice = productDetail.price - (productDetail.price * discount)/100;
                        totalAfterDiscount += newPrice * singleProduct.count;
                    }
                }
                validatedBody.total = total;
                validatedBody.totalAfterDiscount = totalAfterDiscount;
                //get number of km and delivary cost
                let settings = await Setting.find({ deleted: false })
                let kmCost = settings[0].kmCost;
                let numberOfKm = distance(req.body.clientDestination[0],req.body.clientDestination[1],req.body.storeDestination[0],req.body.storeDestination[1],"K")
                console.log(numberOfKm)
                
                validatedBody.delivaryCost = Math.ceil( kmCost * numberOfKm);
                
                validatedBody.finalTotal = totalAfterDiscount + validatedBody.delivaryCost
                console.log(validatedBody.store)
                //
                let createdOrder = await Order.create({ ...validatedBody });
                console.log(createdOrder)
                let order = await Order.populate(createdOrder, populateQuery);
                let reports = {
                    "action":"Create New Order",
                };
                let report = await Report.create({...reports, user: req.user });
                
                sendNotifiAndPushNotifi({////////
                    targetUser: validatedBody.store, 
                    fromUser: validatedBody.client, 
                    text: 'new notification',
                    subject: createdOrder.id,
                    subjectType: 'new order'
                });
                let notif = {
                    "description":'new order'
                }
                Notif.create({...notif,resource:validatedBody.client,target:validatedBody.store,order:createdOrder.id});
        }
            res.status(201).send('done');
        } catch (err) {
            next(err);
            
        }
    },       
 
    validateDeliveryCost() {
        let validations = [
           body('clientDestination').not().isEmpty().withMessage('client Destination is required'),
           body('storeDestination').not().isEmpty().withMessage('target is required'),
        ];
        return validations;
    },
    async getDeliveryCost(req, res, next) {
        try {
            const validatedBody = checkValidations(req);
            let settings = await Setting.find({ deleted: false });
            let kmCost = settings[0].kmCost;
            let numberOfKm = distance(req.body.clientDestination[0],req.body.clientDestination[1],req.body.storeDestination[0],req.body.storeDestination[1],"K")
            console.log(numberOfKm)
            
            let delivaryCost = Math.ceil( kmCost * numberOfKm); 
            console.log(delivaryCost)
            res.send({delivaryCost});
        } catch (err) {
            next(err);
        }
    },
    async preOrderInfo(req, res, next) {
        try {
            let user = req.user;
            await checkExist(req.user._id, User);
            let theUser = await checkExistThenGet(req.user._id, User, { deleted: false }); 
            const validatedBody = checkValidations(req);
           let orders = [];
            let products = validatedBody.productOrders;
            let storesIds = [];
            products.forEach(id => {
                storesIds.push(id.store)
            });  
            storesIds = [ ...new Set(storesIds) ];
            for(var i = 0;i<storesIds.length;i++) { 
                var filterObj = products.filter(function(e) {
                    return e.store == storesIds[i];
                  });
                orders.push(filterObj)
            }
            console.log(orders)
            let allTotal = 0;
            let allTotalAfterDiscount = 0;
            let deliveryCost = 0;
            let allFinalTotal = 0;
            for(var j =0;j<orders.length;j++){
                validatedBody.productOrders = orders[j];
                validatedBody.store = validatedBody.productOrders[0].store;
                let store = await checkExistThenGet(validatedBody.store, User);
                let total = 0;
                let totalAfterDiscount = 0
                for (let singleProduct of validatedBody.productOrders) {
                    let productDetail = await checkExistThenGet(singleProduct.product, Product);
                    total += productDetail.price * singleProduct.count;
                    if(productDetail.hasRule){
                        let rule = await checkExistThenGet(productDetail.rule, Rule, { deleted: false });
                        let discount = rule.discount;
                        let newPrice = productDetail.price - (productDetail.price * discount)/100;
                        totalAfterDiscount += newPrice * singleProduct.count;
                    }else{
                        let arr = store.discounts
                        let obj = arr.find(o => o.package == theUser.package);
                        let index = arr.indexOf(obj);
                        let discount = arr[index].discount;
                        console.log(discount)
                        let newPrice = productDetail.price - (productDetail.price * discount)/100;
                        totalAfterDiscount += newPrice * singleProduct.count;
                    }
                }
                allTotal += total;
                allTotalAfterDiscount += totalAfterDiscount;
                //get number of km and delivary cost
                let settings = await Setting.find({ deleted: false })
                let kmCost = settings[0].kmCost;
                let numberOfKm = distance(req.body.clientDestination[0],req.body.clientDestination[1],req.body.storeDestination[0],req.body.storeDestination[1],"K")
                console.log(numberOfKm)
                
                deliveryCost = Math.ceil( kmCost * numberOfKm);
                
                allFinalTotal += totalAfterDiscount + deliveryCost
               
        }
            res.send({
                total:allTotal,
                totalAfterDiscount:allTotalAfterDiscount,
                deliveryCost:deliveryCost,
                finalTotal:allFinalTotal
            });
        } catch (err) {
            next(err);
            
        }
    },  
    async findById(req, res, next) {
        try {
            let {orderId } = req.params;
            res.send(
                await checkExistThenGet(orderId, Order, { deleted: false, populate: populateQuery })
            );
        } catch (err) {
            next(err);
        }
    },
    async accept(req, res, next) {
        try {
            let { orderId } = req.params;
            let order = await checkExistThenGet(orderId, Order);
            order.status = 'ON_PROGRESS';
            let store = await checkExistThenGet(order.store, User, { deleted: false });
            let user = await checkExistThenGet(order.client, User, { deleted: false }); 
            let orderProducts = order.productOrders;
            let billsItems = []
                for (let singleProduct of orderProducts) {
                    console.log(singleProduct)
                    let productDetail = await checkExistThenGet(singleProduct.product, Product);
                    if(productDetail.hasRule){
                        let rule = await checkExistThenGet(productDetail.rule, Rule, { deleted: false });
                        let discount = rule.discount;
                        console.log(discount)
                        let totalPrice = productDetail.price * singleProduct.count
                        let item = {price:totalPrice}
                        let newPrice = totalPrice - (totalPrice * discount)/100;
                        item.priceAfterDiscount = newPrice;
                        item.discount = discount;
                        item.discountRule = productDetail.rule;
                        console.log(item)
                        billsItems.push(item)
                    }else{
                        let arr = store.discounts
                        let obj = arr.find(o => o.package == user.package);
                        let index = arr.indexOf(obj);
                        let discount = arr[index].discount;
                        console.log(discount)
                        let totalPrice = productDetail.price * singleProduct.count
                        let item = {price:totalPrice}
                        let newPrice = totalPrice - (totalPrice * discount)/100;
                        item.priceAfterDiscount = newPrice;
                        item.discount = discount;
                        console.log(item)
                        billsItems.push(item)
                    }
                }
            
             //bill
            let billBody = {}
            billBody.billItems = billsItems;
            billBody.store = order.store;
            billBody.actionUser = req.user._id;
            billBody.user = order.client;
            billBody.orderBill = true
            console.log(billBody)
            //user points
            let packagee = await checkExistThenGet(user.package, Package, { deleted: false });
            user.points = user.points + packagee.pointsForUser;
            if(packagee.defaultPackage == true){
                if(user.freeOperationsNum >= packagee.freeOperations ){
                    return next(new ApiError(500, ('sorry this user finish all free operation')));
                }else{
                    user.freeOperationsNum = user.freeOperationsNum + 1;
                }
            }
            user.billsCount = user.billsCount + 1; 
            user.save();
            //store points
           
            store.points = store.points + packagee.pointsForStore;
            store.billsCount = store.billsCount + 1;
            store.save();
            //bill action user
            let actionUser = await checkExistThenGet(req.user._id, User, { deleted: false });  
            if(req.user._id != order.store){
                actionUser.billsCount = actionUser.billsCount + 1;
            }
            await actionUser.save()
            
            let createdBill = await Bill.create({ ...billBody});
            order.bill = createdBill.id;
            await order.save();
            
            sendNotifiAndPushNotifi({
                targetUser: order.client, 
                fromUser: req.user, 
                text: 'new notification',
                subject: order.id,
                subjectType: 'your order has been accepted'
            });
            let notif = {
                "description":'your order has been accepted',
                "arabicDescription": 'تم الموافقه على طلبك'
            }

            await Notif.create({...notif,resource:req.user,target:order.client,order:order.id});
            res.send(order);
            
        } catch (error) {
            next(error);
        }
    },
    
   
    async cancel(req, res, next) {
        try {
            let { orderId } = req.params;
            let order = await checkExistThenGet(orderId, Order);
            console.log(order)
            order.status = 'REFUSED';
            order.reason = req.body.reason;
            await order.save();
            let reports = {
                "action":"User cancel order",
            };
            let report = await Report.create({...reports, user: req.user });
            res.send('User cancel order');
            
        } catch (error) {
            next(error);
        }
    },
    
    async onTheWay(req, res, next) {
        try {
            let { orderId } = req.params;
            let order = await checkExistThenGet(orderId, Order);
            order.status = 'ON_THE_WAY';
            await order.save();
            sendNotifiAndPushNotifi({
                targetUser: order.client, 
                fromUser: req.user, 
                text: 'new notification',
                subject: order.id,
                subjectType:' your order on the wy to you'
            });
            let notif = {
                "description": 'your order on the way to you',
                "arabicDescription": "طلبك فى الطريق اليك "
            }
            await Notif.create({...notif,resource:req.user,target:order.client,order:order.id});
            res.send(order);
            
        } catch (error) {
            next(error);
        }
    },
    async arrived(req, res, next) {
        try {
            let { orderId } = req.params;
            let order = await checkExistThenGet(orderId, Order);
            order.status = 'ARRIVED';
            await order.save();
            sendNotifiAndPushNotifi({
                targetUser: order.client, 
                fromUser: req.user, 
                text: 'new notification',
                subject: order.id,
                subjectType: 'your order has been arrived to you'
            });
            let notif = {
                "description":'your order has been arrived to you',
                "arabicDescription": 'لقد تم ايصال طلبك'
            }

            await Notif.create({...notif,resource:req.user,target:order.client,order:order.id});
            let user = await checkExistThenGet(order.client, User);
            res.send(order);
            
        } catch (error) {
            next(error);
        }
    },
    async delivered(req, res, next) {
        try {
            let { orderId } = req.params;
            let order = await checkExistThenGet(orderId, Order);
            if(order.status == 'DELIVERED')
            return next(new ApiError(400, ('this order is finished'))) 
            order.status = 'DELIVERED';
            await order.save();
            
            sendNotifiAndPushNotifi({
                targetUser: order.client, 
                fromUser: req.user, 
                text: 'new notification',
                subject: order.id,
                subjectType: ' delivered your order done !'
            });
            let notif = {
                "description":' delivered your order done !',
                "arabicDescription":" تم توصيل طلبك "
            }
            await Notif.create({...notif,resource:req.user,target:order.client,order:order.id});
            let reports = {
                "action":"Order Deliverd",
            };
            let report = await Report.create({...reports, user: req.user });
            res.send(order);
            
        } catch (error) {
            next(error);
        }
    },
  
    async rate(req, res, next) {
        try {
            let { orderId } = req.params;
            let order = await checkExistThenGet(orderId, Order);
            order.rate = req.body.rate;
            order.rateText = req.body.rateText;
            await order.save();
           
            sendNotifiAndPushNotifi({
                targetUser: order.driver, 
                fromUser: req.user, 
                text: 'new notification',
                subject: order.id,
                subjectType: 'user rate your service'
            });
            let notif = {
                "description":'user rate your service',
                "arabicDescription":"تم تقييم خدمتك"
            }
            await Notif.create({...notif,resource:req.user,target:order.driver,order:order.id});
            res.send(order);
            
        } catch (error) {
            next(error);
        }
    },
   
    async delete(req, res, next) {
        try {
            let { orderId } = req.params;
            let order = await checkExistThenGet(orderId, Order);
            order.deleted = true;
            await order.save();
            let reports = {
                "action":"Delete Order",
            };
            let report = await Report.create({...reports, user: req.user });
            res.status(204).send();
        } catch (error) {
            next(error)
        }
    },
    
    async update(req, res, next) {
        try {
            let {orderId} = req.params
            let order = await checkExistThenGet(orderId, Order,{deleted:false});
            if(order.status != 'DELIVERED')
            return next(new ApiError(400, ('this order is finished'))) 

            let user = await checkExistThenGet(order.client, User, { deleted: false }); 
            const validatedBody = checkValidations(req);
            let store = await checkExistThenGet(order.store, User);
            let total = 0;
            let totalAfterDiscount = 0
            for (let singleProduct of validatedBody.productOrders) {
                let productDetail = await checkExistThenGet(singleProduct.product, Product);
                total += productDetail.price * singleProduct.count;
                if(productDetail.hasRule){
                    let rule = await checkExistThenGet(productDetail.rule, Rule, { deleted: false });
                    let discount = rule.discount;
                    let newPrice = productDetail.price - (productDetail.price * discount)/100;
                    totalAfterDiscount += newPrice * singleProduct.count;
                }else{
                    let arr = store.discounts
                    let obj = arr.find(o => o.package == user.package);
                    let index = arr.indexOf(obj);
                    let discount = arr[index].discount;
                    console.log(discount)
                    let newPrice = productDetail.price - (productDetail.price * discount)/100;
                    totalAfterDiscount += newPrice * singleProduct.count;
                }
            }
            validatedBody.total = total;
            validatedBody.totalAfterDiscount = totalAfterDiscount;
            //get number of km and delivary cost
            let settings = await Setting.find({ deleted: false })
            let kmCost = settings[0].kmCost;
            let numberOfKm = distance(req.body.clientDestination[0],req.body.clientDestination[1],req.body.storeDestination[0],req.body.storeDestination[1],"K")
            console.log(numberOfKm)
            
            validatedBody.delivaryCost = Math.ceil( kmCost * numberOfKm);
            
            validatedBody.finalTotal = totalAfterDiscount + validatedBody.delivaryCost
            console.log(validatedBody.store)
            //
            let updatedOrder = await Order.findByIdAndUpdate(orderId, {
                ...validatedBody,
            }, { new: true });
            order = await checkExistThenGet(orderId, Order);
            if(order.status != 'PENDING'){
                let orderProducts = order.productOrders;
                let billsItems = []
                    for (let singleProduct of orderProducts) {
                        console.log(singleProduct)
                        let productDetail = await checkExistThenGet(singleProduct.product, Product);
                        if(productDetail.hasRule){
                            let rule = await checkExistThenGet(productDetail.rule, Rule, { deleted: false });
                            let discount = rule.discount;
                            console.log(discount)
                            let totalPrice = productDetail.price * singleProduct.count
                            let item = {price:totalPrice}
                            let newPrice = totalPrice - (totalPrice * discount)/100;
                            item.priceAfterDiscount = newPrice;
                            item.discount = discount;
                            item.discountRule = productDetail.rule;
                            console.log(item)
                            billsItems.push(item)
                        }else{
                            let arr = store.discounts
                            let obj = arr.find(o => o.package == user.package);
                            let index = arr.indexOf(obj);
                            let discount = arr[index].discount;
                            console.log(discount)
                            let totalPrice = productDetail.price * singleProduct.count
                            let item = {price:totalPrice}
                            let newPrice = totalPrice - (totalPrice * discount)/100;
                            item.priceAfterDiscount = newPrice;
                            item.discount = discount;
                            console.log(item)
                            billsItems.push(item)
                        }
                    }
                
                //bill
                let billBody = {}
                billBody.billItems = billsItems;
                let updatedBill = await Bill.findByIdAndUpdate(order.bill, {
                    ...billBody,
                });
            }
            order = await Order.populate(updatedOrder, populateQuery);
            res.status(201).send(order);
        } catch (err) {
            next(err);
            
        }
    },  
  
};
module.exports = OrderController;