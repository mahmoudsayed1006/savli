import express from 'express';
import OrderController from '../../controllers/order/order.controller';
import { requireAuth } from '../../services/passport';
import { multerSaveTo } from '../../services/multer-service';

const router = express.Router();

router.route('/')
    .post(
        requireAuth,
        OrderController.validateCreated(),
        OrderController.create
    );
router.route('/preOrderInfo')
    .post(
        requireAuth,
        OrderController.validateCreated(),
        OrderController.preOrderInfo
    );

router.route('/')
    .get(OrderController.findOrders)

router.route('/getDeliveryCost')
    .post(OrderController.validateDeliveryCost(),OrderController.getDeliveryCost)
router.route('/:orderId')
    .get(OrderController.findById)
    .put(requireAuth,OrderController.update)
    .delete( requireAuth,OrderController.delete);
router.route('/:orderId/cancel')
    .put( requireAuth,OrderController.cancel)

router.route('/:orderId/accept')
    .put( requireAuth,OrderController.accept)

router.route('/:orderId/onTheWay')
    .put( requireAuth,OrderController.onTheWay)

router.route('/:orderId/arrived')
    .put( requireAuth,OrderController.arrived)  

router.route('/:orderId/delivered')
    .put( requireAuth,OrderController.delivered)  

router.route('/:orderId/rate')
    .put( requireAuth,OrderController.rate)  
 
export default router;
