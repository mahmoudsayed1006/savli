import express from 'express';
import BillController from '../../controllers/bills/bills.controller';
import { requireAuth } from '../../services/passport';
import { parseStringToArrayOfObjectsMw } from '../../utils';

const router = express.Router();

router.route('/')
    .get(BillController.findAll);

router.route('/:user/createBill')
    .post(
        requireAuth,
        BillController.validateBody(),
        //parseStringToArrayOfObjectsMw('billItems'),
        BillController.create
    )   
router.route('/:billId')
    .get(BillController.findById)
    .delete( requireAuth,BillController.delete)
    .put(
        requireAuth,
        //parseStringToArrayOfObjectsMw('billItems'),
        BillController.validateBody(true),
        BillController.update
    ) 


router.route('/:billId/cancel')
    .put(requireAuth,BillController.cancel)

router.route('/:billId/rate')
    .post(
        requireAuth,
        BillController.ratValidateBody(),
        BillController.rate
    )   

router.route('/findAllRate/rate') 
    .get(
        //requireAuth,
        BillController.findAllRate
    )   

router.route('/:userId/getCount')
    .get(
        requireAuth,
        BillController.getCount
    )   

export default router;