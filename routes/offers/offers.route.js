import express from 'express';
import {  requireAuth} from '../../services/passport';
import OfferController from '../../controllers/offers/offers.controller';
import { multerSaveTo } from '../../services/multer-service';

const router = express.Router();

router.route('/')
    .post(  
        requireAuth,
        multerSaveTo('Offers').single('img'),
        OfferController.validateBody(),
        OfferController.create
    ).get(OfferController.findAll);

    
router.route('/:offerId')
    .put(
        requireAuth,
        OfferController.validateBody(true),
        OfferController.update
    )
    .get(OfferController.findById)
    .delete( requireAuth,OfferController.delete);
export default router;
