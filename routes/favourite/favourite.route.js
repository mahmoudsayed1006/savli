import express from 'express';
import FavouriteController from '../../controllers/favourite/favourite.controller';
import { requireAuth } from '../../services/passport';

const router = express.Router();

router.route('/:favPerson/list')
    .post(
        requireAuth,
        FavouriteController.create
    );
router.route('/:userId/users')
    .get(FavouriteController.findAll);
    
router.route('/:favPerson')
    .delete( requireAuth,FavouriteController.unFavourite);







export default router;