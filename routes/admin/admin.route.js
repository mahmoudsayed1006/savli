import express from 'express';
import adminController from '../../controllers/admin/admin.controller';

const router = express.Router();
router.route('/users')
    .get(adminController.getLastUser);

router.route('/actions')
    .get(adminController.getLastActions);

router.route('/count')
    .get(adminController.count);


router.route('/:userId/countStore')
    .get(adminController.countStore);

router.route('/:userId/countUser')
    .get(adminController.countUser);

router.route('/graph')
    .get(adminController.graph);

router.route('/:storeId/graphStore')
    .get(adminController.graphStore);


export default router;
