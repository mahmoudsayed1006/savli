import mongoose, { Schema } from "mongoose";
import autoIncrement from 'mongoose-auto-increment';
const NotifSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    resource: {
        type: Number,
        ref: 'user'
    },
    target: {
        type: [Number],
        ref: 'user'
    },
    description:{
        type:String
    },
    arabicDescription:{
        type:String
    },
    fav:{
        type:Number,
        ref:'favourite'
    },
    bill:{
        type:Number,
        ref:'bill'
    },
    rate:{
        type:Number,
        ref:'rate'
    },
    read:{
        type:Boolean,
        default:false
    },
    deleted: {
        type: Boolean,
        default: false
    }
}, { timestamps: true });

NotifSchema.index({ location: '2dsphere' });
NotifSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        if (ret.destination) {
            ret.destination = ret.destination.coordinates;
        }
    }
});
autoIncrement.initialize(mongoose.connection);
NotifSchema.plugin(autoIncrement.plugin, { model: 'notif', startAt: 1 });

export default mongoose.model('notif', NotifSchema);