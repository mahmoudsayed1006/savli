import twilio from 'twilio';
import config from '../config';
import { generateVerifyCode } from './generator-code-service';

export function sendConfirmCode(phone) {
    let client = new twilio(config.twilio.accountSid, config.twilio.authToken);
    let generatedVerifyCode = generateVerifyCode();

    client.messages.create({
        body: 'رمز التفعيل الخاص بك هو : ' + generatedVerifyCode,
        to: phone,
        from:/* '+12408216481'*/'Savli App'
    }).then((message) => {
        console.log(message);
    }).catch(err => console.log('Twilio Error: ', err))
    
    return generatedVerifyCode;
} 

export function sendForgetPassword(password, phone) {
    let client = new twilio(config.twilio.accountSid, config.twilio.authToken);
    client.messages.create({
        body: 'رمز التفعيل الخاص بك هو : '+ password,
        to: phone /*phone*/,
        from: 'Savli App'
    }).then((message) => {
        console.log(message);
    }).catch(err => console.log('Twilio Error: ', err))
}