import mongoose,{ Schema } from "mongoose";
import autoIncrement from 'mongoose-auto-increment';
const OfferSchema=new Schema({
    _id: {
        type: Number,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    img: {
        type: String,
        required: true
    },
    store: {
        type: Number,
        required: true,
        ref:'user'
    },
    offerPackage:{
        type: Number,
        required: true,
        ref:'offerPackage'
    },
    fromDate: {
        type: Date,
        required: true
    },
    toDate: {
        type: Date,
        required: true
    },
    toDateMillSec: {
        type: Number,
        required: true
    },
    end: {
        type:Boolean,
        default:false
    },
    special: {
        type:Boolean,
        default:false
    },
    deleted:{
        type:Boolean,
        default:false
    }
},{timestamps:true});
OfferSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
OfferSchema.plugin(autoIncrement.plugin, { model: 'offer', startAt: 1 });

export default mongoose.model('offer', OfferSchema);