import Product from "../../models/product/product.model";
import ApiResponse from "../../helpers/ApiResponse";
import { handleImgs, checkValidations  ,distance} from "../shared/shared.controller";
import { checkExistThenGet, checkExist, isArray } from "../../helpers/CheckMethods";
import { body } from "express-validator/check";
import User from "../../models/user/user.model";
import Report from "../../models/reports/report.model";
import { toImgUrl } from "../../utils";
import Cart from "../../models/cart/cart.model";

import Notif from "../../models/notif/notif.model";
import { sendNotifiAndPushNotifi } from "../../services/notification-service";
import ApiError from '../../helpers/ApiError';

const populateQuery = [
    { path: 'store', model: 'user' },
];
const populateQuery2 = [
    { path: 'user', model: 'user' }
];
export default {
    async findAll(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            {available, name,store,sortByPrice} = req.query;
            let query = {deleted: false };
  
            if(available == "true") query.available = true;
            if(available == "false") query.available = false;
            if(name) {
                query = {
                    $and: [
                        { $or: [
                            {name_en: { $regex: '.*' + name + '.*' , '$options' : 'i'  }}, 
                            {name_ar: { $regex: '.*' + name + '.*' , '$options' : 'i'  }}, 
                          
                          ] 
                        },
                        {deleted: false},
                    ]
                };
            }
            
            if (store) query.store = store;
            let sortd = {createdAt: -1}
            if (sortByPrice) sortd = {price:1};
            let products = await Product.find(query).populate(populateQuery)
                .sort(sortd)
                .limit(limit)
                .skip((page - 1) * limit);

            const productsCount = await Product.count(query);
            const pageCount = Math.ceil(productsCount / limit);
            res.send(new ApiResponse(products, page, pageCount, limit, productsCount, req));
        } catch (err) {
            next(err);
        }
    },
    validateCreatedProduct(isUpdate = false) {
        
        let validations = [
            body('name_en').not().isEmpty().withMessage('name_en is required'),
            body('name_ar').not().isEmpty().withMessage('name_ar is required'),
            body('store').not().isEmpty().withMessage('store is required')
            .isNumeric().withMessage('numeric value required'),
            body('description').not().isEmpty().withMessage('description is required'),
            body('price').not().isEmpty().withMessage('price is required')
            .isNumeric().withMessage('price numeric value required'),
            body('rule').optional().isNumeric().withMessage('rule numeric value required'),
            body('properties').optional().custom(vals => isArray(vals)).withMessage('properties should be an array')
                .isLength({ min: 1 }).withMessage('properties should have at least one element of properties')
                .custom(async (properties, { req }) => {
                    for (let prop of properties) {
                        body('attr').not().isEmpty().withMessage('attr is required'),
                        body('value').not().isEmpty().withMessage('value is required')
                    }
                    return true;
                }),
            
            
        ];
    
        return validations;
    },
    async create(req, res, next) {
        try {
            let user = req.user; 
            
            const validatedBody = checkValidations(req);
            if(validatedBody.rule){
                validatedBody.hasRule = true
            }
            let img = await handleImgs(req);
            console.log(validatedBody.properties)
            let product = await Product.create({
                ...validatedBody,
                img: img, 
            });
            let reports = {
                "action":"Create New Product",
            };
            let report = await Report.create({...reports, user: req.user });
            res.status(201).send(product);
            
        } catch (err) {
            next(err);
        }
    },
    async findById(req, res, next) {
        try {
            let { productId } = req.params;

            await checkExist(productId, Product,
                { deleted: false });
            let product = await Product.findById(productId).populate(populateQuery);
            res.send(
                product
            );
        } catch (err) {
            next(err);
        }
    },

   
    async active(req, res, next) {
        try {
            let user = req.user;

            let {productId} = req.params;
            let product = await checkExistThenGet(productId, Product,
                {deleted: false });
            product.available = true;
            await product.save();
            let reports = {
                "action":"Active Product",
            };
            let report = await Report.create({...reports, user: user });
            res.send('product active');
            
        } catch (error) {
            next(error);
        }
    },

    async disactive(req, res, next) {
        try {
            let user = req.user;

            let {productId } = req.params;
            let product = await checkExistThenGet(productId, Product,
                {deleted: false });

            product.available = false;
            await product.save();
            let reports = {
                "action":"Dis-active Product",
            };
            let report = await Report.create({...reports, user: user });
            res.send('product dis-active');
        } catch (error) {
            next(error);
        }
    },
    
    async update(req, res, next) {
        try {
            
            let {productId } = req.params;
            let user = req.user;
            await checkExist(productId, Product,
                {deleted: false });

            
            const validatedBody = checkValidations(req);
             if(validatedBody.rule){
                validatedBody.hasRule = true
            }
            if (req.files) {
                if (req.files['img']) {
                    let imagesList = [];
                    for (let imges of req.files['img']) {
                        imagesList.push(await toImgUrl(imges))
                    }
                    validatedBody.img = imagesList;
                }
            }

            let updatedProduct = await Product.findByIdAndUpdate(productId, {
                ...validatedBody,

            }, { new: true }).populate(populateQuery);
            
           

            let reports = {
                "action":"Update Product",
            };
            let report = await Report.create({...reports, user: req.user });
            res.status(200).send(updatedProduct);
        }
        catch (err) {
            next(err);
        }
    },
    
    async delete(req, res, next) {
        try {
            let {productId } = req.params;

            let product = await checkExistThenGet(productId, Product,
                {deleted: false });
            let cartes = await Cart.find({ product: productId });
            if(cartes){
                for (let cart of cartes ) {
                    cart.deleted = true;
                    await cart.save();
                }
            }
            product.deleted = true
            await product.save();
            let reports = {
                "action":"Delete Product",
            };
            let report = await Report.create({...reports, user: req.user._id });
            res.status(204).send();
        }
        catch (err) {
            next(err);
        }
    },
    
}