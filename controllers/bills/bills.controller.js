import ApiResponse from "../../helpers/ApiResponse";
import Bill from "../../models/bills/bills.model";
import User from "../../models/user/user.model";
import Report from "../../models/reports/report.model";
import ApiError from '../../helpers/ApiError';
import { checkExist, checkExistThenGet, isImgUrl } from "../../helpers/CheckMethods";
import { handleImg, checkValidations } from "../shared/shared.controller";
import { body } from "express-validator/check";
import { sendNotifiAndPushNotifi } from "../../services/notification-service";
import Notif from "../../models/notif/notif.model";
import Rate from "../../models/rate/rate.model";
import Package from "../../models/package/package.model";
import Rule from "../../models/rules/rules.model";

const populateQuery = [
    {
        path: 'store', model: 'user',
        populate: { path: 'category', model: 'category' },
    },
    {
        path: 'store', model: 'user',
        populate: { path: 'subCategory', model: 'category' },
    },
    {
        path: 'store', model: 'user',
        populate: { path: 'city', model: 'city' },
    },
    {
        path: 'store', model: 'user',
        populate: { path: 'area', model: 'area' },
    },
    {
        path: 'user', model: 'user',
        populate: { path: 'category', model: 'category' },
    },
    {
        path: 'user', model: 'user',
        populate: { path: 'subCategory', model: 'category' },
    },
    {
        path: 'user', model: 'user',
        populate: { path: 'city', model: 'city' },
    },
    {
        path: 'user', model: 'user',
        populate: { path: 'area', model: 'area' },
    },
    { path:'actionUser',modal:'user'},
    {path:'billItems.discountRule',modal:'rule'}

];
const populateQuery2 = [
    {
        path: 'store', model: 'user',
        populate: { path: 'category', model: 'category' },
    },
    {
        path: 'store', model: 'user',
        populate: { path: 'subCategory', model: 'category' },
    },
    {
        path: 'store', model: 'user',
        populate: { path: 'city', model: 'city' },
    },
    {
        path: 'store', model: 'user',
        populate: { path: 'area', model: 'area' },
    },
    {
        path: 'user', model: 'user',
        populate: { path: 'category', model: 'category' },
    },
    {
        path: 'user', model: 'user',
        populate: { path: 'subCategory', model: 'category' },
    },
    {
        path: 'user', model: 'user',
        populate: { path: 'city', model: 'city' },
    },
    {
        path: 'user', model: 'user',
        populate: { path: 'area', model: 'area' },
    },

];
export default {

    async findAll(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            {store,user,bill,actionUser,cancel} = req.query;
            let query = { deleted: false };
            if(store) query.store = store;
            if(user) query.user = user;
            if(bill) query.bill = bill;
            if(cancel == "true") query.cancel = true
            if(cancel == "false") query.cancel = false
            if(actionUser) query.actionUser = actionUser;
            let bills = await Bill.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);
            for (let bill of bills) {
                let theBill = await checkExistThenGet(bill._id, Bill);
                let item ={};
                let billItems = [];

                item.price = theBill.price;
                item.discount = theBill.discount;
                item.priceAfterDiscount = theBill.priceAfterDiscount;
                if(theBill.billItems.length == 0){
                    billItems.push(item)
                    theBill.billItems = billItems;
                    await theBill.save()
                }
            }
            bills = await Bill.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);
            const billsCount = await Bill.count(query);
            const pageCount = Math.ceil(billsCount / limit);

            res.send(new ApiResponse(bills, page, pageCount, limit, billsCount, req));
        } catch (err) {
            next(err);
        }
    },
    async getCount(req, res, next) {

        try {
            let {userId} = req.params;
            let user = await checkExistThenGet(userId, User, { deleted: false }); 
            let {cancel} = req.query
            let query = { deleted: false };
            query = {
                $or: [
                    {user: userId},
                    {store: userId},
                ]
            };
            if(cancel == "true") query.cancel = true
            if(cancel == "false") query.cancel = false
            let bills = await Bill.find(query)

            var price = 0;
            for (var i = 0; i < bills.length; i++) { 
                let billItems = bills[i].billItems;
                for (var j = 0; j < billItems.length; j++) { 
                    price += billItems[j].price
                }
            }

            var priceAfterDiscount = 0;
            for (var i = 0; i < bills.length; i++) { 
                let billItems = bills[i].billItems;
                for (var j = 0; j < billItems.length; j++) { 
                    priceAfterDiscount += billItems[j].priceAfterDiscount
                }
            }
            res.send({
                price:price,
                priceAfterDiscount:priceAfterDiscount,
                discountAmount:price - priceAfterDiscount,
                userPoint:user.points
            });
        } catch (err) {
            next(err);
        }
    },

    validateBody(isUpdate = false) {
        let validations = [
            body('price').optional(),
            body('billItems')
            .isLength({ min: 1 }).withMessage('billItems should have at least one element of billItems')
            .custom(async (properties, { req }) => {
                for (let prop of properties) {
                    body('price').not().isEmpty().withMessage('price is required'),
                    body('discountRule').optional()
                }
                return true;
            }),
        ];
        
        return validations;
    },

    async create(req, res, next) {

        try {
            const validatedBody = checkValidations(req);
            let storeId = req.user._id;
            if(req.user.type == "STORE"){
                validatedBody.store = req.user._id;
                storeId = req.user._id;
            }else{
                validatedBody.store = req.user.store;
                storeId = req.user.store;
            }
            validatedBody.actionUser = req.user._id
            let store = await checkExistThenGet(storeId, User, { deleted: false });
            let user = await checkExistThenGet(req.params.user, User, { deleted: false });  
            let billsItems = []
            for (let singleBill of validatedBody.billItems) {   
                console.log(singleBill.discountRule)
                if(singleBill.discountRule){
                    let rule = await checkExistThenGet(singleBill.discountRule, Rule, { deleted: false });
                    let discount = rule.discount;
                    console.log(discount)
                    let item = {price:singleBill.price}
                    let newPrice = singleBill.price - (singleBill.price * discount)/100;
                    item.priceAfterDiscount = newPrice;
                    item.discount = discount;
                    item.discountRule = singleBill.discountRule;
                    console.log(item)
                    billsItems.push(item)
                }else{
                    let arr = store.discounts
                    let obj = arr.find(o => o.package == user.package);
                    let index = arr.indexOf(obj);
                    let discount = arr[index].discount;
                    console.log(discount)
                    let item = {price:singleBill.price}
                    let newPrice = singleBill.price - (singleBill.price * discount)/100;
                    item.priceAfterDiscount = newPrice;
                    item.discount = discount;
                    console.log(item)
                    billsItems.push(item)
                }

            }
            validatedBody.billItems = billsItems
            if(validatedBody.price){
                let arr = store.discounts
                let obj = arr.find(o => o.package == user.package);
                let index = arr.indexOf(obj);
                let discount = arr[index].discount;
                console.log(discount)
                let newPrice = validatedBody.price - (validatedBody.price * discount)/100;
                validatedBody.priceAfterDiscount = newPrice;
                validatedBody.discount = discount;
            }
            let packagee = await checkExistThenGet(user.package, Package, { deleted: false });
               
                
            validatedBody.user = req.params.user;
            user.points = user.points + packagee.pointsForUser;
            if(packagee.defaultPackage == true){
                if(user.freeOperationsNum >= packagee.freeOperations ){
                    return next(new ApiError(500, ('sorry this user finish all free operation')));
                }else{
                    user.freeOperationsNum = user.freeOperationsNum + 1;
                }
            }
            user.billsCount = user.billsCount + 1; 
            user.save();

            store.points = store.points + packagee.pointsForStore;
            store.billsCount = store.billsCount + 1;
            store.save();

            let actionUser = await checkExistThenGet(req.user._id, User, { deleted: false });  
            if(req.user._id != storeId){
                actionUser.billsCount = actionUser.billsCount + 1;
            }
            await actionUser.save()
            
            let createdBill = await Bill.create({ ...validatedBody});
            sendNotifiAndPushNotifi({
                targetUser: req.params.user, 
                fromUser: storeId, 
                text: 'new notification',
                subject: createdBill.id,
                subjectType: 'new bill'
            });
            let notif = {
                "description":'new bill',
                "arabicDescription":'فاتوره جديده'
            }
            await Notif.create({...notif,resource:storeId,target:req.params.user,bill:createdBill.id});
            let reports = {
                "action":"Create Bill",
            };
            let report = await Report.create({...reports, user: user});

            res.status(201).send(createdBill);
        } catch (err) {
            next(err);
        }
    },
    async update(req, res, next) {

        try {
            const validatedBody = checkValidations(req);
            let{billId} = req.params
            let theBill = await checkExistThenGet(billId, Bill, { deleted: false });
            let storeId = req.user._id;
            if(req.user.type == "STORE"){
                validatedBody.store = req.user._id;
                storeId = req.user._id;
            }else{
                validatedBody.store = req.user.store;
                storeId = req.user.store;
            }
            validatedBody.actionUser = req.user._id
            let store = await checkExistThenGet(storeId, User, { deleted: false });
            let user = await checkExistThenGet(theBill.user, User, { deleted: false });
            let billsItems = []
            let billItems = theBill.billItems;
            for (let [index, singleBill] of validatedBody.billItems.entries()) {   
                let discount = billItems[index].discount;
                let item = {price:singleBill.price}
                let newPrice = singleBill.price - (singleBill.price * discount)/100;
                item.priceAfterDiscount = newPrice;
                item.discount = discount;
                if(billItems[index].discountRule){
                    item.discountRule = billItems[index].discountRule;
                }
                billsItems.push(item)
            }
            validatedBody.billItems = billsItems
            /*
            let discount = bill.discount
            let newPrice = validatedBody.price - (validatedBody.price * discount)/100;
            validatedBody.priceAfterDiscount = newPrice;
            validatedBody.discount = bill.discount;
*/
            let updatedBill = await Bill.findByIdAndUpdate(billId, {
                ...validatedBody,
            });
            sendNotifiAndPushNotifi({
                targetUser: theBill.user, 
                fromUser: storeId, 
                text: 'new notification',
                subject: updatedBill.id,
                subjectType: 'update bill'
            });
            let notif = {
                "description":'update bill',
                "arabicDescription":' تعديل الفاتوره '
            }
            await Notif.create({...notif,resource:storeId,target:theBill.user,bill:updatedBill.id});
            let reports = {
                "action":"Update Bill",
            };
            let report = await Report.create({...reports, user: user});

            res.status(201).send(await Bill.findById(updatedBill.id).populate(populateQuery));
        } catch (err) {
            next(err);
        }
    },


    async cancel(req, res, next) {

        try {
            const validatedBody = checkValidations(req);
            let{billId} = req.params
            let bill = await checkExistThenGet(billId, Bill, { deleted: false });
            if(bill.cancel == true ){
                return next(new ApiError(500, ('this bill has been canceled')));
            }
            let storeId = req.user._id;
            if(req.user.type == "STORE"){
                validatedBody.store = req.user._id;
                storeId = req.user._id;
            }else{
                validatedBody.store = req.user.store;
                storeId = req.user.store;
            }
            validatedBody.actionUser = req.user._id
            let store = await checkExistThenGet(storeId, User, { deleted: false });
            let user = await checkExistThenGet(bill.user, User, { deleted: false });     
            let packagee = await checkExistThenGet(user.package, Package, { deleted: false });     
                user.points = user.points - packagee.pointsForUser;
                if(packagee.defaultPackage == true){
                    if(user.freeOperationsNum >= 0 ){
                        user.freeOperationsNum = user.freeOperationsNum - 1;
                    }
                }
            user.billsCount = user.billsCount - 1; 
            user.save();

            store.points = store.points - packagee.pointsForStore;
            store.billsCount = store.billsCount - 1;
            store.save();

            let actionUser = await checkExistThenGet(req.user._id, User, { deleted: false });  
            if(req.user._id != storeId){
                actionUser.billsCount = actionUser.billsCount - 1;
            }
            await actionUser.save()
            bill.cancel = true
            await bill.save()
            sendNotifiAndPushNotifi({
                targetUser: bill.user, 
                fromUser: storeId, 
                text: 'new notification',
                subject: billId,
                subjectType: 'cancel bill'
            });
            let notif = {
                "description":'cancel bill',
                "arabicDescription":' تم الغاء الفاتوره '
            }
            await Notif.create({...notif,resource:storeId,target:bill.user,bill:billId});
            let reports = {
                "action":"cancel Bill",
            };
            let report = await Report.create({...reports, user: user});

            res.status(201).send(bill);
        } catch (err) {
            next(err);
        }
    },


    async findById(req, res, next) {
        try {
            let { billId } = req.params;
            await checkExist(billId,Bill, { deleted: false });
            let bills = await Bill.findById(billId).populate(populateQuery);
            res.send(bills);
        } catch (err) {
            next(err);
        }
    },
    
    async delete(req, res, next) {
        try {
            let user = req.user;
            let { billId } = req.params;
            let bills = await checkExistThenGet(billId, Bill, { deleted: false });
            bills.deleted = true;
            await bills.save();
            let reports = {
                "action":"Delete bills",
            };
            let report = await Report.create({...reports, user: user ,belong:'SHOP'});
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },
    ratValidateBody(isUpdate = false) {
        let validations = [
            body('comment').not().isEmpty().withMessage('comment is required'),
            body('rate').not().isEmpty().withMessage('rate is required')
            .isNumeric().withMessage('rate numeric value required'),
            
        ];
        return validations;
    },
    async rate(req, res, next) {
        try {
            let { billId } = req.params;
            let bill = await checkExistThenGet(billId, Bill, { deleted: false });
            bill.hasRate = true;
            await bill.save();
            const validatedBody = checkValidations(req);
            validatedBody.bill = billId;
            validatedBody.user = req.user._id;
            validatedBody.store = bill.store;
            let d = new Date();
            let dateMilleSec = Date.parse(d)
            validatedBody.date = dateMilleSec;
            let rateCreated = await Rate.create({ ...validatedBody });

             //rate
            let user = await checkExistThenGet(bill.store, User, { deleted: false });
            let newRate = user.rateCount + parseInt(validatedBody.rate);
            user.rateCount = newRate;
            user.rateNumbers = user.rateNumbers + 1;
            let totalDegree = user.rateNumbers * 5; 
            let degree = newRate * 100
            let ratePrecent = degree / totalDegree;
            let rate = ratePrecent / 20
            console.log(ratePrecent)
            user.rate = Math.ceil(parseInt(rate));
            console.log( user.rate) 
            user.save();
            sendNotifiAndPushNotifi({
                targetUser: bill.store, 
                fromUser: req.user, 
                text: 'new notification',
                subject: rateCreated.id,
                subjectType: 'new rate'
            });
            let notif = {
                "description":'you have a new rate',
                "arabicDescription":'لديك تقييم جديد'
            }
            await Notif.create({...notif,resource:req.user,target:bill.store,rate:rateCreated.id});
            res.status(200).send("rate success")

        }
        catch (err) {
            next(err);
        }
    },
    async findAllRate(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let { billId,store } = req.query;
            let query = {deleted: false};
            if(billId)
                query.bill = billId
            if(store)
                query.store = store
            let billRate = await Rate.find(query).populate(populateQuery2)
                .sort({createdAt: -1})
                .limit(limit)
                .skip((page - 1) * limit);


            const billCount = await Rate.count(query);
            const pageCount = Math.ceil(billCount / limit);

            res.send(new ApiResponse(billRate, page, pageCount, limit, billCount, req));
        } catch (err) {
            next(err);
        }
    },
   
};