import express from 'express';
import userRoute from './user/user.route';
import ContactRoute  from './contact/contact.route';
import CategoryRoute  from './category/category.route';

import ReportRoute  from './reports/report.route';
import NotifRoute  from './notif/notif.route';
import AdminRoute  from './admin/admin.route';
import AboutRoute  from './about/about.route';
import CityRoute  from './city/city.route';
import FavouriteRoute  from './favourite/favourite.route';
import ProblemRoute  from './problem/problem.route';
import PackageRoute  from './package/package.route';
import BillRoute  from './bills/bills.route';
import OfferRoute  from './offers/offers.route';
import OfferPackageRoute  from './offer package/offerPackage.route';
import RulesRoute  from './rules/rules.route';
import CartRoute  from './cart/cart.route';
import ProductsRoute  from './product/product.route';
import SettingRoute  from './setting/setting.route';
import OrdersRoute  from './order/order.route';

import { requireAuth } from '../services/passport';

const router = express.Router();
router.use('/categories',CategoryRoute);
router.use('/offers',OfferRoute);

router.use('/bills',BillRoute);
router.use('/cities',CityRoute);
router.use('/', userRoute);
router.use('/contact-us',ContactRoute);
router.use('/reports',requireAuth, ReportRoute);
router.use('/notif',requireAuth, NotifRoute);
router.use('/admin',requireAuth, AdminRoute);
router.use('/about',AboutRoute);
router.use('/favourite', FavouriteRoute);
router.use('/problem', ProblemRoute);
router.use('/package', PackageRoute);
router.use('/offerPackages', OfferPackageRoute);
router.use('/discountsRules', RulesRoute);
router.use('/products', ProductsRoute);
router.use('/cart', CartRoute);
router.use('/setting', SettingRoute);
router.use('/orders', OrdersRoute);
export default router;
