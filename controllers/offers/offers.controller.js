import ApiResponse from "../../helpers/ApiResponse";
import { checkExist, checkExistThenGet, isImgUrl } from "../../helpers/CheckMethods";
import { handleImg, checkValidations } from "../shared/shared.controller";
import { body } from "express-validator/check";
import Offer from "../../models/offers/offers.model";
import User from "../../models/user/user.model";
import Package from "../../models/offer package/offerPackage.model";


const populateQuery = [ 
    { path: 'store', model: 'user' },
    { path: 'offerPackage', model: 'offerPackage' },
];

export default {


    async findAll(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            { store,special} = req.query;
            
            let query = { deleted: false};
            if (store)
                query.store = store;

            if (special)
                query.special = special;
           
            let offers = await Offer.find(query).populate(populateQuery)
                .sort({createdAt: -1})
                .limit(limit)
                .skip((page - 1) * limit);
            for (let offer of offers) {
                let theOffer = await checkExistThenGet(offer._id, Offer)
                let curr = Date.parse(new Date());
                console.log(curr)
                let to = Date.parse(theOffer.toDate)
                console.log(to)
                if(curr > to ){
                    theOffer.end = true
                }
                await theOffer.save()
            }
            const offersCount = await Offer.count(query);
            const pageCount = Math.ceil(offersCount / limit);

            res.send(new ApiResponse(offers, page, pageCount, limit, offersCount, req));
        } catch (err) {
            next(err);
        }
    },
    
    validateBody(isUpdate = false) {
        let validations = [
            body('description').not().isEmpty().withMessage('description is required'),
            body('toDate').not().isEmpty().withMessage('start Date is required'),
            body('fromDate').not().isEmpty().withMessage('end Date is required'),
            body('store').not().isEmpty().withMessage('store is required')
            .isNumeric().withMessage("store should be number"),
            body('offerPackage').not().isEmpty().withMessage('offerPackage is required')
            .isNumeric().withMessage("offerPackage should be number"),
        ];
        if (isUpdate)
            validations.push([
                body('img').not().isEmpty().withMessage('offer image is required').custom(val => isImgUrl(val)).withMessage('img should be a valid img')
            ]);

        return validations;
    },

    async create(req, res, next) {
        try {
            const validatedBody = checkValidations(req);
            await checkExist(validatedBody.store, User, { deleted: false });
            let offerPackage = await checkExistThenGet(validatedBody.offerPackage, Package, { deleted: false });
            if(offerPackage.special == true){
                validatedBody.special = true
            }
            if(req.user.type == "STORE"){
                let user = await checkExistThenGet(req.user_id, User, { deleted: false });
                if(user.points < offerPackage.points){
                    return next(new ApiError(500, ('sorry your points not enough')));
                }else{
                    user.points = user.points - offerPackage.points;
                    await user.save()
                }
               
            }
            validatedBody.toDateMillSec = Date.parse(validatedBody.toDate)
            let image = await handleImg(req);
            validatedBody.img = image;
            let createdoffer = await Offer.create({ ...validatedBody});
            res.status(201).send(createdoffer);
        } catch (err) {
            next(err);
        }
    },


    async findById(req, res, next) {
        try {
            let { offerId } = req.params;
            await checkExist(offerId, Offer, { deleted: false });
            let offer = await Offer.findById(offerId).populate(populateQuery)
            res.send(offer);
        } catch (err) {
            next(err);
        }
    },

    async update(req, res, next) {

        try {
            let { offerId } = req.params;
            await checkExist(offerId, Offer, { deleted: false });

            const validatedBody = checkValidations(req);

            if (req.file) {
                let image = await handleImg(req, { attributeName: 'img', isUpdate: true });
                validatedBody.img = image;
            }

            let updatedoffer = await Offer.findByIdAndUpdate(offerId, {
                ...validatedBody,
            }, { new: true });

            res.status(200).send(updatedoffer);
        }
        catch (err) {
            next(err);
        }
    },

    async delete(req, res, next) {
        try {
            let { offerId } = req.params;
            let offer = await checkExistThenGet(offerId, Offer, { deleted: false });
            offer.deleted = true;
            await offer.save();
            res.status(204).send();

        }
        catch (err) {
            next(err);
        }
    },



};