import mongoose, { Schema } from "mongoose";
import autoIncrement from 'mongoose-auto-increment';
const OrderSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    client: {
        type: Number,
        ref: 'user',
        required:true
    },
    store: {
        type: Number,
        ref: 'user',
        required:true
    },
    bill: {
        type: Number,
        ref:'bill',
    },
    total: {
        type: Number,
        required: true
    },
    totalAfterDiscount:{
        type: Number,
        required: true
    },
    finalTotal:{
        type: Number,
        required: true,
    },
    delivaryCost: {
        type: Number,
        required: true
    },
    clientDestination: {
        type: [Number] ,
        required: true
    },
    storeDestination: {
        type: [Number] ,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    city: {
        type: Number,
        ref:'city',
        required: true
    },
    area: {
        type: Number,
        ref:'area',
    },
    status: {
        type: String,
        enum: ['PENDING', 'ON_PROGRESS','ON_THE_WAY', 'REFUSED','ARRIVED', 'DELIVERED'],
        default: 'PENDING'
    },

    accept:{
        type:Boolean,
        default:false
    },
    productOrders: [
        new Schema({
            product: {
                type: Number,
                ref: 'product',
                required: true
            },
            store: {
                type: Number,
                ref: 'user',
                required: true
            },
            count: {
                type: Number,
                default: 1
            },
        }, { _id: false })
    ],
    reason:{
        type:String
    },

    deleted: {
        type: Boolean,
        default: false
    }
}, { timestamps: true });

OrderSchema.index({ location: '2dsphere' });
OrderSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        if (ret.destination) {
            ret.destination = ret.destination.coordinates;
        }
    }
});
autoIncrement.initialize(mongoose.connection);
OrderSchema.plugin(autoIncrement.plugin, { model: 'order', startAt: 1 });

export default mongoose.model('order', OrderSchema);