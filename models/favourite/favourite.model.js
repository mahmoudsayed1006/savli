import mongoose, { Schema } from "mongoose";
import { isImgUrl } from "../../helpers/CheckMethods";
import autoIncrement from 'mongoose-auto-increment';
const FavouriteSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    user: {
        type: Number,
        ref: 'user',
    },
    favPerson: {
        type: Number,
        ref: 'user'
    },
    deleted: {
        type: Boolean,
        default: false
    }
}, { timestamps: true });

FavouriteSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
FavouriteSchema.plugin(autoIncrement.plugin, { model: 'favourite', startAt: 1 });

export default mongoose.model('favourite', FavouriteSchema);